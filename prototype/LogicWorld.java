package prototype;
import java.util.ArrayList;
import java.lang.Runtime;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.scene.layout.Pane;

import javax.sound.midi.SysexMessage;

//represents a certain world. Consists of a list of premises that hold in the world.
public class LogicWorld
{
	public String worldName = "<Unnamed>"; //a string to identify this world

	private final String inputFilePath = "prover9_files/input.in"; //location of the file where premises will be written to
	private final String outputFilePath = "prover9_files/output.out";

	private final String axiomFile = "prover9_files/modelaxioms.txt"; //file where model axioms are stored

	private final String modelinPath = "prover9_files/model.in"; //location of files where models are constructed
	private final String modeloutPath = "prover9_files/model.out";

	private ArrayList<Premise> premises; //the list of premises added so far
	private ArrayList<LogicShape> shapes; //the list of shapes that make up this world
	private int noShapes = 0; //the number of shapes in the world

	private Pane worldPane; //the javafx pane this world will be displayed in
							//can be set to null, if we don't intend to display this world

	int timeLimit; //the maximum amount of time in secs that prover9 can spend on proofs/model building

	//CONSTRUCTOR
	//create an empty world
	public LogicWorld(String givenName, Pane displayPane)
	{
		this(givenName, displayPane, 30);
	}

	public LogicWorld(String givenName, Pane displayPane, int givenTimeLimit)
	{
		worldName = givenName;
		premises = new ArrayList<Premise>();
		shapes = new ArrayList<LogicShape>();
		worldPane = displayPane;
		timeLimit = givenTimeLimit;
	}

	//create a world that satisfies the given premise(s) (optionally with a time limit)

	public LogicWorld(String givenName, Pane displayPane, ArrayList<Premise> givenPremises) throws ModelException
	{
		this(givenName, displayPane, givenPremises, 30);
	}
	public LogicWorld(String givenName, Pane displayPlane, ArrayList<Premise> givenPremises, int givenTimeLimit) throws ModelException
	{
		worldName = givenName;
		timeLimit = givenTimeLimit;
		worldPane = displayPlane;

		//write the premises and axioms
		try
		{
			createModelInputFile(givenPremises);
		}
		catch (IOException e)
		{
			System.out.println(e.toString());
		}

		//now the model goals are written, we can run mace4
		Runtime rt = Runtime.getRuntime();
		String cmd = "mace4 -f " + modelinPath;
		System.out.println("Running: " + cmd);

		try
		{
			Process p = rt.exec(cmd);
			int exitValue = p.waitFor();

			//if the result is anything but successful...
			if (exitValue != 0)
			{
				throw new ModelException("Model could not be built");
			}

			BufferedReader stdin = new BufferedReader(new InputStreamReader(p.getInputStream()));

			//write the output of the model to a temporary file
			PrintWriter tempOut = new PrintWriter(new File("prover9_files/temp.txt"));

			String s = null;

			while ((s = stdin.readLine()) != null)
			{
				//write the output to the file
				tempOut.println(s);
			}

			tempOut.close();
			stdin.close();

			//convert the model into the 'portable' format using interpformat
			String intercmd = "interpformat portable -f prover9_files/temp.txt";
			System.out.println("Running: " + intercmd);
			p = rt.exec(intercmd);
			exitValue = p.waitFor();

			//read the output and write to the model.out file
			stdin = new BufferedReader(new InputStreamReader(p.getInputStream()));

			PrintWriter modelOut = new PrintWriter(new File(modeloutPath));

			String line = null;
			while ((line = stdin.readLine()) != null)
			{
				modelOut.println(line);
			}

			stdin.close();
			modelOut.close();

		}
		catch (Exception e)
		{
			throw new ModelException("No model could be found");
		}

		//parse the output file
		ModelParser mp = new ModelParser(modeloutPath);
		ArrayList<Premise> temp = mp.parse();

		premises = temp;

		noShapes = mp.getNoConsts();

		//create the shapes
		shapes = new ArrayList<LogicShape>();
		createShapes();


	}

	//END OF CONSTRUCTORS

	//creates a file with the given premises ready to be input into mace4
	private void createModelInputFile(ArrayList<Premise> givenPremises) throws IOException
	{
		//setup writer to model input file
		File outputfile = new File(modelinPath);
		PrintWriter modelOut = new PrintWriter(outputfile);

		//set time limit
		modelOut.println("assign(max_seconds, " + Integer.toString(timeLimit) + ").");
		modelOut.println("formulas(assumptions).");


		//write the axioms to model file
		writeAxioms(modelOut);

		//write the premises themselves
		for (Premise curPremise: givenPremises)
		{
			modelOut.println(curPremise.toString());
		}

		modelOut.println("end_of_list.");

		modelOut.close();

	}

	//adds a premise to the world
	public void addPremise(Premise newPremise)
	{
		premises.add(newPremise);
	}

	//adds a whole array of premises
	public void addPremises(ArrayList<Premise> givenPremises)
	{
		for (Premise newPremise: givenPremises)
		{
			premises.add(newPremise);
		}
	}

	public void addShape(LogicShape givenShape)
	{
		shapes.add(givenShape);
		noShapes = shapes.size();
	}

	public void deleteShape(LogicShape toDelete)
	{
		shapes.remove(toDelete);
		noShapes = shapes.size();
	}

	public void clearShapes()
	{
		shapes.clear();
		noShapes = 0;
	}

	//removes all premises and shapes
	public void clearPremises()
	{
		premises.clear();
	}


	//prints all the premises
	public void listPremises()
	{
		for (Premise p: premises)
		{
			System.out.println(p.toString());
		}
	}

	public ArrayList<Premise> getAllPremises()
	{
		return premises;
	}

	//returns all the unary premises (isBig(), isCircle etc.)
	public ArrayList<Premise> getUnaryPremises()
	{
		ArrayList<Premise> temp = new ArrayList<Premise>();
		for (Premise prem: premises)
		{
			if (prem.isUnary())
			{
				temp.add(prem);
			}
		}

		return temp;
	}

	//returns all binary premises
	public ArrayList<Premise> getBinaryPremises()
	{
		ArrayList<Premise> temp = new ArrayList<Premise>();
		for (Premise prem: premises)
		{
			if (prem.isBinary())
			{
				temp.add(prem);
			}
		}

		return temp;
	}

	//returns all unary premises that contain the given constant
	public ArrayList<Premise> getUnaryPremises(String constName)
	{
		//get all the unary premises first
		ArrayList<Premise> unaryPrems = this.getUnaryPremises();

		ArrayList<Premise> temp = new ArrayList<Premise>();

		for (Premise prem: unaryPrems)
		{
			//if the premise contains the constant, add it to temp
			if (prem.contains(constName))
			{
				temp.add(prem);
			}
		}

		return temp;
	}

	//returns all binary premises that contain both of the given constants
	public ArrayList<Premise> getBinaryPremises(String first, String second)
	{
		//get all binary premises
		ArrayList<Premise> binPrems = this.getBinaryPremises();
		ArrayList<Premise> temp = new ArrayList<Premise>();

		for (Premise prem: binPrems)
		{
			if (prem.contains(first) && prem.contains(second))
			{
				temp.add(prem);
			}
		}

		return temp;
	}

	//opens the modelAxioms file and writes its contents to the given PrintWriter
	private void writeAxioms(PrintWriter out) throws IOException
	{
		//setup reader from axiom file
		BufferedReader axiomsIn = new BufferedReader(new FileReader(axiomFile));

		//write each line of the axiom file to the output
		String curLine = null;

		while ((curLine = axiomsIn.readLine()) != null)
		{
			out.println(curLine);
		}

		axiomsIn.close();
	}

	//returns an array of all the names of the shapes
	private String[] getAllNames()
	{
		String[] names = new String[shapes.size()];

		for (int i = 0; i < shapes.size(); i++)
		{
			names[i] = shapes.get(i).getConstName();
		}

		return names;
	}


	//writes the premises and given conclusion to the prover9 input file
	private void writePremises(Premise conc) throws IOException
	{
			//open the output file
			File outputfile = new File(inputFilePath);
			PrintWriter output = new PrintWriter(outputfile);

			output.println("assign(max_seconds, " + Integer.toString(timeLimit) + ").");
			output.println("formulas(sos).");

			//write the premises of the world
			for (Premise cur: premises)
			{
				output.println("\t" + cur.toString());
			}


			output.println("end_of_list.\n");

			//write the conclusion i.e. goal
			output.println("formulas(goals).");
			conc = removeUniversal(conc);
			output.println("\t" + conc.toString());
			output.println("end_of_list.\n");


			output.close();
	}

	public Premise removeUniversal(Premise givenPremise)
	{
		String premText = givenPremise.toString();

		//check for two nested alls i.e. all x all y isLeftOf(x,y).
		Pattern twoAll = Pattern.compile("\\ball\\b.*\\ball\\b.\\b");
		Matcher matcher = twoAll.matcher(premText);


		if (matcher.find())
		{
			//find where "all" is in the string
			int allAt = matcher.start();
			allAt += 4;
			//the first bound var is 4 characters after
			char boundVar1 = premText.charAt(allAt);

			//the second is 5 characters after that
			allAt += 6;
			char boundVar2 = premText.charAt(allAt);
			allAt += 2;

			//isolate the section bound by the quantifier
			String boundSect = Premise.isolateQuantifier(premText, allAt);

			String restOfPrem = premText.replaceAll(boundSect, "");

			//get the list of the names of shapes
			String[] shapeNames = getAllNames();

			int noShapes = getNoShapes();

			Premise finalPremise = null;

			for (int i = 0; i < noShapes; i++ )
			{
				String name1 = shapeNames[i];

				for (int j = i + 1; j < noShapes; j++)
				{


					String name2 = shapeNames[j];

					//replace "boundvar1" with "name1" and "boundvar2" with "name2"
					String newText = boundSect.replaceAll(Character.toString(boundVar1), name1);
					newText = newText.replaceAll(Character.toString(boundVar2), name2);

					if (finalPremise == null)
					{
						finalPremise = new Premise(newText);
					}
					else
					{
						Premise temp = new Premise(newText);
						finalPremise = new Premise(finalPremise, Connective.AND, temp);
					}


				}

				Premise temp = new Premise(restOfPrem);
				finalPremise = new Premise(finalPremise, Connective.AND, temp);

				return finalPremise;

			}
		}

		Pattern oneAll = Pattern.compile("\\ball\\b");
		matcher = oneAll.matcher(premText);

		//if an "all" is found...
		if (matcher.find())
		{

			//find the bound variable (the second character after "all")

			//find where "all" is in the string
			int allAt = matcher.start();

			//the bound variable is 4 character after the start of "all"
			allAt += 4;
			char boundVar = premText.charAt(allAt);

			allAt += 2;

			//isolate the section bound by this quantifier
			String boundSect = Premise.isolateQuantifier(premText, allAt);

			String restOfPrem = premText.replaceAll(boundSect, "");
			System.out.println("REST: " + restOfPrem);
			System.out.println("ISOLATED: " + boundSect);

			//remove the first instance of the bound variable
			//at the same time, remove the word "all, remove the space after it, and the period at the end of the statement
			//premText = premText.substring(0, allAt - 4) + premText.substring(allAt + 2, premText.length() -1);


			//create a copy of the premise for each element in the domain (each shape)
			int noShapes = getNoShapes();

			//get the list of the names of shapes
			String[] shapeNames = getAllNames();

			Premise finalPremise = null;
			//for each name
			for (int i = 0; i < noShapes; i++)
			{
				//get the name of the shape
				String curShapeName = shapeNames[i];

				//replace every instance of the bound variable with the name of the shape
				String newText = boundSect.replaceAll(Character.toString(boundVar), curShapeName);

				//replace this new text in the full premise
				premText = premText.replaceAll(boundSect, newText);

				//if the final premise has not been initialised yet...
				if (finalPremise == null)
				{
					//create a new premise
					finalPremise = new Premise(newText);
				}
				else
				{
					//else append the premise to the existing one using AND
					Premise temp = new Premise(newText);
					finalPremise = new Premise(finalPremise, Connective.AND, temp);
				}

			}

			Premise temp = new Premise(restOfPrem);
			finalPremise = new Premise(finalPremise, Connective.AND, temp);

			return (finalPremise);


		}
		else
		{
			return (givenPremise);
		}
	}

	public int getNoShapes()
	{
		return noShapes;
	}


	//checks if the set of premises logically entail the given premise
	public boolean logicallyEntails(Premise conc)
	{
		boolean result = false;
		try
		{
			//write the premises and conclusion
			writePremises(conc);

			//get the java runtime object and run the command
			Runtime rt = Runtime.getRuntime();
			String cmd = "prover9 -f " + inputFilePath;
			System.out.println("Running Prover9. Output written to: " + outputFilePath);


			//open the output file ready to write to
			BufferedWriter out = new BufferedWriter(new FileWriter(outputFilePath, true));

			Process p = rt.exec(cmd);

			//the return value will tell us if the proof was successful or not
			int exitValue = p.waitFor();

			switch(exitValue)
			{
				case 0: //statement is true
					result = true;
					//System.out.println("Statement is true!");
					break;

				case 1: case 102: //prover9 had an error
					System.out.println("Prover9 error!");
					break;

				case 2: //no valid input (statement is false)
					//System.out.println("No items on screen");
					break;

				case 3: case 4: case 5: case 6: case 7: //prover9 gave up. (statement is not true)
					//System.out.println("Statement is not true");
					break;

			}



			BufferedReader stdin = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String s = null;

			while ((s = stdin.readLine()) != null)
			{
				System.out.println(s);
				out.write(s + "\n");
				out.flush();
			}

		}
		catch (Exception e)
		{
			System.out.println("ERROR: Could not read from/write to prover9_files");
		}

		return result;


	}

	//creates a set of shapes that satisfy the premises of this world
	private void createShapes()
	{
		//if no Pane was given to display the shapes in, this function can't be used
		if (worldPane == null)
		{
			return;
		}

		//get the number of shapes/constants of the world
		int noShapes = getNoShapes();

		//for each constant, we need to create a shape
		for (int i = 0; i < noShapes; i++)
		{
			//create a name for this shape
			String constName = "c" + Integer.toString(i);

			int shapeSize = -1;
			int shape = -1;

			//find any premises that involve this shape
			ArrayList<Premise> premises = getUnaryPremises(constName);

			//create hypotheses of the possible size and shape of this shape
			Premise isBig = new Premise(Predicate.IS_BIG, constName);
			Premise isMed = new Premise(Predicate.IS_MED, constName);
			Premise isSmall = new Premise(Predicate.IS_SMALL, constName);



			Premise isCircle = new Premise(Predicate.IS_CIRCLE, constName);
			Premise isSquare = new Premise(Predicate.IS_SQUARE, constName);

			for (Premise curPremise: premises)
			{

				//check if this premise can confirm a hypothesis
				if (curPremise.PremEquals(isBig))
				{
					shapeSize = 3;
				}
				else if (curPremise.PremEquals(isMed))
				{
					shapeSize = 2;
				}
				else if (curPremise.PremEquals(isSmall))
				{
					shapeSize = 1;
				}
				else if (curPremise.PremEquals(isCircle))
				{
					shape = 0;
				}
				else if (curPremise.PremEquals(isSquare))
				{
					shape = 1;
				}

			}

			//we should now have identified the shapes size and shape
			LogicShape newShape = null;

			switch(shape)
			{
				case 0: newShape = new LogicCircle(worldPane,shapeSize);
					break;

				case 1: newShape = new LogicSquare(worldPane, shapeSize);
					break;
			}

			if (newShape != null)
			{
				//assign the constant to this shape so we can identify it later
				newShape.setConstName(constName);

				//add the shape to the list of shapes in this world
				shapes.add(newShape);
			}
		}

		return;

	}

	//gets a list of the shapes of this world
	public ArrayList<LogicShape> getShapes()
	{
		return shapes;
	}

	//gets an array of the shapes (combined into sets) of this world in left-to-right order
	public ShapeSet[] getHOrder()
	{
		//create a temporary copy of the shapes and binary premises of this world
		ArrayList<LogicShape> workingList = new ArrayList<LogicShape>(shapes);
		ArrayList<Premise> workingPremises = getBinaryPremises();

		//array of the shapes stored in left to right order
		LogicShape[] HOrder = new LogicShape[shapes.size()];

		//continually find the leftmost shape, until all shapes have been ordered

		int i;

		for (i = 0; i < shapes.size(); i++)
		{
			LogicShape leftMost = findMost(Predicate.IS_LEFT_OF, workingList, workingPremises);

			//set this shape as the ith leftmost
			HOrder[i] = leftMost;

			//remove this shape
			workingList.remove(leftMost);

			//remove any premises that contain this shape
			ListIterator<Premise> itr = workingPremises.listIterator();
			while(itr.hasNext())
			{
				Premise curPrem = itr.next();
				if (curPrem.contains(leftMost.getConstName()))
				{
					itr.remove();
				}
			}
		}

		//find all shapes that are horizontally aligned and clump them together
		ArrayList<ShapeSet> HSets = ShapeSet.clumpShapes(shapes, this, Orientation.HORIZONTAL);

		//replace each entry in horizontal order array by a shape set
		ShapeSet[] HSetOrder = new ShapeSet[HSets.size()];

		//for each set, find the shape that matches it
		for (i = 0; i < HSetOrder.length; i++)
		{
			ShapeSet curSet = HSets.get(i);

			for (int j = 0; j < HSetOrder.length; j++)
			{
				if (curSet.contains(HOrder[j]))
				{
					//we've found the matching shape
					//so the current set is in position j
					HSetOrder[j] = curSet;
				}
			}
		}

		return HSetOrder;
	}


	//gets an array of the shapes (combined into sets) of this world sorted into top-to-bottom order
	public ShapeSet[] getVOrder()
	{
		//get a copy of the shapes and premises of the world
		ArrayList<LogicShape> workingList = new ArrayList<LogicShape>(shapes);
		ArrayList<Premise> workingPremises = getBinaryPremises();

		LogicShape[] VOrder = new LogicShape[shapes.size()];
		int i;
		for (i = 0; i < shapes.size(); i++)
		{
			LogicShape highest = findMost(Predicate.IS_ABOVE, workingList, workingPremises);

			//set this shape as the ith highest
			VOrder[i] = highest;

			//remove this shape
			workingList.remove(highest);

			//remove any premises that contain this shape
			ListIterator<Premise> itr = workingPremises.listIterator();
			while(itr.hasNext())
			{
				Premise curPrem = itr.next();
				if (curPrem.contains(highest.getConstName()))
				{
					itr.remove();
				}
			}
		}

		//find all shapes that are vertically aligned and clump them together
		ArrayList<ShapeSet> VSets = ShapeSet.clumpShapes(shapes, this, Orientation.VERTICAL);

		//replace each entry in horizontal order array by a shape set
		ShapeSet[] VSetOrder = new ShapeSet[VSets.size()];

		//for each set, find the shape that matches it
		for (i = 0; i < VSetOrder.length; i++)
		{
			ShapeSet curSet = VSets.get(i);

			for (int j = 0; j < VSetOrder.length; j++)
			{
				if (curSet.contains(VOrder[j]))
				{
					//we've found the matching shape
					//so the current set is in position j
					VSetOrder[j] = curSet;
				}
			}
		}

		return VSetOrder;
	}

	//finds the shape that is most "x" in this world
	//i.e. if givenPredicate = isLeftOf, it will find the shape that is the furthest to the left
	//used when determining the order of shapes
	private static LogicShape findMost(Predicate givenPredicate, ArrayList<LogicShape> givenShapes, ArrayList<Premise> givenPremises)
	{
		//our current most constant
		String curMost = givenShapes.get(0).getConstName(); //initialise it to the first shape

		for (Premise prem: givenPremises)
		{
			//if the premise is not of the given type or negated, we can skip it
			if (!prem.containsPredicate(givenPredicate))
			{
				continue;
			}
			if (prem.containsConnective(Connective.NOT))
			{
				continue;
			}


			//get the arguments from the premise
			ArrayList<String> arguments = prem.getArgs();

			//the first element in the list is left of the second
			//so if the second one equals our current most...
			//we know the first one must be more

			if (arguments.get(1).equals(curMost))
			{
				//update the current left most
				curMost = arguments.get(0);
			}

		}

		//find the shape that matches the name
		LogicShape result = null;
		for (LogicShape shape: givenShapes)
		{
			if (shape.getConstName().equals(curMost))
			{
				result = shape;
				break;
			}
		}

		return result;
	}

}
