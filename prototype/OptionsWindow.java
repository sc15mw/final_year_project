package prototype;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OptionsWindow
{
	MainApp parent;

	Stage options;
	CheckBox allowMultLabelsBox;
	TextField timeLimitBox;

	//opens a new options window
	public OptionsWindow(MainApp givenParent)
	{
		parent = givenParent;
		//set up the dialog box
		setUpUI();

	}

	private void setUpUI()
	{
		options = new Stage();
		options.initModality(Modality.APPLICATION_MODAL);
		VBox root = new VBox();
		root.setSpacing(5);


		Text msg = new Text("Options");

		allowMultLabelsBox = new CheckBox("Allow multiple labels with same name");
		allowMultLabelsBox.setSelected(parent.allowMultLabels);

		//contains the box for setting the time limit and its label
		HBox timeLimitSection = new HBox();
		timeLimitBox = new TextField();
		timeLimitBox.setText(Integer.toString(parent.timeLimit));

		timeLimitSection.getChildren().addAll(new Text("Time Limit (secs)"), timeLimitBox);

		Button closeBtn = new Button("Save & Close");
		closeBtn.setOnAction(saveOptions);


		Text resultMsg = new Text("");

		root.getChildren().addAll(msg, allowMultLabelsBox, timeLimitSection, closeBtn, resultMsg);
		Scene ds = new Scene(root, 300, 200);
		options.setScene(ds);
	}

	public void display()
	{
		options.show();
	}

	//saves the options currently selected to the parent
	EventHandler<ActionEvent> saveOptions = new EventHandler<ActionEvent>()
	{
		@Override
		public void handle(ActionEvent event)
		{
			parent.allowMultLabels = allowMultLabelsBox.isSelected();

			//get the time limit from the user (may not be a number)
			int newTimeLimit;

			try
			{
				newTimeLimit = Integer.parseInt(timeLimitBox.getText());
			}
			catch (NumberFormatException e)
			{
				//if the user's entry is not an integer, leave the time limit as it is
				newTimeLimit = parent.timeLimit;
			}

			parent.timeLimit = newTimeLimit;
			options.close();

		}
	};




}
