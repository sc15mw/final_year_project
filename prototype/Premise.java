package prototype;

import java.util.ArrayList;

//represents a premise which can be used in a LogicWorld
//equivalent to a wff
public class Premise
{
	String premText; //the premise written in prolog syntax
	ArrayList<String> arguments; //the arguments of the premise
	ArrayList<Connective> connectives; //the connectives in this premise
	ArrayList<Quantifier> quantifiers; //the quantifiers in this premise

	//CONSTRUCTORS for a single predicate
	//constructor for unary predicate e.g. isBig(1)
	public Premise(Predicate type, String arg1)
	{
		//construct the prolog statement
		premText = type + "(" + arg1 + ").";

		//add the arguments to the list
		arguments = new ArrayList<String>();
		arguments.add(arg1);

		connectives = new ArrayList<Connective>();
		quantifiers = new ArrayList<Quantifier>();
	}
	//constructor for binary premise e.g. isOverlapping(1, 2)
	public Premise(Predicate type, String arg1, String arg2)
	{
		premText = type + "(" + arg1 + "," + arg2 + ").";
		//add the arguments to the list
		arguments = new ArrayList<String>();
		arguments.add(arg1);
		arguments.add(arg2);

		connectives = new ArrayList<Connective>();
		quantifiers = new ArrayList<Quantifier>();
	}

	//connecting existing premises
	public Premise(Premise a, Connective con, Premise b)
	{
		//remove the . from both existing statements
		String stra = a.toString().replace(".", "");
		String strb = b.toString().replace(".", "");

		//build the new premise
		premText = "(" + stra + " " + con + " " + strb + ").";

		//get all the arguments from both premises and add them to this one
		arguments = new ArrayList<String>();
		arguments.addAll(a.getArgs());
		arguments.addAll(b.getArgs());

		connectives = new ArrayList<Connective>();
		connectives.addAll(a.getConnectives());
		connectives.addAll(b.getConnectives());
		connectives.add(con);

		quantifiers = new ArrayList<Quantifier>();
		quantifiers.addAll(a.getQuantifiers());
		quantifiers.addAll(b.getQuantifiers());

	}

	public Premise(Connective con, Premise a)
	{

		//the only connective allowed for one premise is NOT
		if (con.equals(Connective.NOT))
		{
			String text = a.toString().replace(".", "");
			premText = con + "(" + text + ").";
			arguments = new ArrayList<String>();
			arguments.addAll(a.getArgs());

			connectives = new ArrayList<Connective>();
			connectives.addAll(a.getConnectives());
			connectives.add(con);

			quantifiers = new ArrayList<Quantifier>();
			quantifiers.addAll(a.getQuantifiers());
		}
		else if (con.equals(Connective.NOT))
		{
			String temp  = a.toString();
			temp = temp.replace(".", "");

			premText = con + "(" + temp + ").";
			arguments = new ArrayList<String>();
			arguments.addAll(a.getArgs());

		}
	}

	public Premise(Quantifier quan, String boundVar, Premise a)
	{
		premText = quan + " " + boundVar + " " + a.toString();
		arguments = new ArrayList<String>();
		arguments.addAll(a.getArgs());

		//add a's existing connectives
		connectives = new ArrayList<Connective>();
		connectives.addAll(a.getConnectives());

		//add a's existing quantifiers plus the new one
		quantifiers = new ArrayList<Quantifier>();
		quantifiers.addAll(a.getQuantifiers());
		quantifiers.add(quan);
	}

	//create a premise matching the given string
	//(NOTE: the caller assumes responsibility for the correctness of the premise)
	public Premise(String givenString)
	{
		premText = givenString;

		arguments = new ArrayList<String>();
		connectives = new ArrayList<Connective>();
		quantifiers = new ArrayList<Quantifier>();
		//find the arguments of this string

		//scan through the string to find where the arguments start and end
		int argStartPos = -1;
		int argEndPos = -1;
		for (int i = 0; i < premText.length(); i++)
		{
			if (premText.charAt(i) == '(')
			{
				argStartPos = i;
			} else if (premText.charAt(i) == ')')
			{
				argEndPos = i;
			}
		}
		//we now have the contents between the parentheses
		String temp = premText.substring(argStartPos + 1, argEndPos);

		//if the string does not contain a comma, it is unary
		if (!premText.contains(","))
		{
			//there is only one argument, so it is the whole of 'temp'
			arguments.add(temp);
		}
		else
		{
			//binary premise
			//split the string into two arguments
			String[] tempArray = temp.split(",");

			//trim each argument and add it
			arguments.add(tempArray[0].trim());
			arguments.add(tempArray[1].trim());
		}
	}

	public ArrayList<Connective> getConnectives() {
		return connectives;
	}

	public ArrayList<Quantifier> getQuantifiers()
	{
		return quantifiers;
	}

	//given the text of a premise and the starting point of the bound section
	//returns the substring that is bound by the quantifier
	//e.g. "all x isBig(x)" returns "isBig(x)"
	public static String isolateQuantifier(String premText, int start)
	{
		int pos = start;

		//pos is now at the start of section bound by the quantifier
		//if there is no parenthesis,
		//the section continues until the next connective OR period (end of statement)
		char curChar = premText.charAt(pos);

		if (curChar != '(')
		{
			//note: "-" (hypen) is used to identify "->" (implies)
			// - cannot be "not" since that would be a syntax error here
			while (curChar != '&' && curChar != '|' && curChar != '-' && curChar != '.')
			{
				pos++;
				curChar = premText.charAt(pos);
			}

			//now the end of the section bound by the variable has been reached

			String result = premText.substring(start, pos);
			result = result.trim();
			return (result);
		}
		else
		{
			//move past the parenthesis
			pos++;
			start = pos;



			//keep going until the matching parenthesis is found

			int openPar = 1; //no of unmatched parentheses

			while (openPar != 0)
			{
				//get next character
				curChar = premText.charAt(pos);

				//if a new open parenthesis is found,
				//we're looking for one more closing parenthesis
				if (curChar == '(')
				{
					openPar++;
				}

				//if the last character was a closing parenthesis, and we had an unmatched open parenthesis
				//the matching parenthesis has now been found
				if (curChar == ')' && openPar != 0)
				{
					openPar--;
				}
				pos++;
			}

			String result = premText.substring(start, pos -1);
			result = result.trim();
			return (result);

		}

	}

	//gets the text of the premise as a prolog statement
	@Override
	public String toString()
	{
		return premText;
	}

	//checks if two premises are equal
	public boolean PremEquals(Premise other)
	{
		if (this.toString().equals(other.toString()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//checks if the premise contains the given constant name
	public boolean contains(String constName)
	{
		if (this.toString().contains(constName))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//checks if the premise is of the given type
	public boolean containsPredicate(Predicate givenPredicate)
	{
		if (this.isAtomic() && this.contains(givenPredicate.toString()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean containsConnective(Connective givenConnective)
	{
		for (Connective conn: connectives)
		{
			if (givenConnective.equals(conn))
			{
				return true;
			}
		}
		return false;
	}

	//returns true if the premise contains a quantifier
	public boolean containsQuantifier(Quantifier q)
	{
		for (Quantifier quant: quantifiers)
		{
			if (quant.equals(q))
			{
				return true;
			}
		}
		return false;
	}

	//returns all the arguments of this premise as an array of strings
	//the arguments are in the same order as they appear in the premise
	public ArrayList<String> getArgs()
	{
		return arguments;
	}

	//returns if this premise is unary or not
	public boolean isUnary()
	{
		if (arguments.size() == 1 && isAtomic())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean isBinary()
	{
		if (arguments.size() == 2 && isAtomic())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//indicates if the premise is atomic i.e. made of only one predicate
	public boolean isAtomic()
	{
		//if it contains any connectives, it cannot be atomic
		return (connectives.isEmpty());

	}



	//checks if the given string is a valid prolog statement
	public static boolean checkIfValid(String statement)
	{
		Premise test = new Premise(statement);
		//split statement into predicates/premises (we can only test for atomic statements)
		/*
		if (!test.isAtomic())
		{
			System.out.println("NOT ATOMIC!");
			String[] temp;
			if (test.containsConnective(Connective.AND))
			{
				temp = statement.split(Connective.AND.toString());
				for (int i = 0; i < temp.length; i++)
				{
					System.out.println(temp[i]);
				}
			}
			else if (test.containsConnective(Connective.OR))
			{
				temp = statement.split(Connective.OR.toString());
			}
			else if (test.containsConnective(Connective.IMPLIES))
			{
				temp =  statement.split(Connective.IMPLIES.toString());
			}



		}
		*/
		//the statement should have the following format: quantifier boundvariable predicate(arg1, arg2).
		//arg2 and the quantifier is optional

		//check if the statement ends with a "."
		if (!statement.endsWith("."))
		{
			return false;
		}


		//remove the "."
		statement = statement.replaceAll("[.]", "");

		//check if the statement starts with any predicate
		Predicate[] preds = Predicate.getAll();
		Predicate predFound = null;

		for (int i = 0; i < preds.length; i++)
		{
			if (statement.startsWith(preds[i].toString()))
			{
				predFound = preds[i];
			}
		}

		//no predicate found
		if (predFound == null)
		{
			return false;
		}

		//remove the predicate
		statement = statement.replace(predFound.toString(), "");

		//statement should contain either 1 or 2 arguments between parentheses
		//an argument can be one or more letters or numbers
		//each argument should be separated by a comma
		String arg = "[a-zA-Z0-9]+";

		String oneArg = "[(]" + arg + "[)]";
		String twoArg = "[(]" + arg + "[,]" + arg + "[)]";

		if (statement.matches(oneArg) || statement.matches(twoArg))
		{
			return true;
		}
		else
		{
			return false;
		}

	}


}
