package prototype;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.*;

import javafx.scene.layout.Pane;
import javafx.scene.Group;
import javafx.scene.control.Label;

import javafx.scene.paint.Color;
import javafx.geometry.Bounds;

import java.util.ArrayList;

//a subclass of circle, adding extra methods for testing logic
public class LogicCircle extends LogicShape
{
	//FIELDS
	private Circle shape; //the visual circle that this shape corresponds to
	private int size; //holds the size of this circle
									 //NOT actual size but (1 = small, 2 = medium, 3 = big)

	private Pane parentPane; //the pane that is displaying this circle
	private Text label; //reference to the label of this circle

	private String constName; //the name of this shape (usually the same as displayed in the label)


	//CONSTRUCTOR
	public LogicCircle(Pane givenParent, int givenSize)
	{
		//create a default circle using javafx
		shape = new Circle();

		//set the parent
		parentPane = givenParent;

		//select the size
		size = givenSize;
		double radius = 0.0;
		switch(givenSize)
		{
			case 1: //small circle
				radius = 30.0;
				break;
			case 2: //medium
				radius = 50.0;
				break;
			case 3: //big
				radius = 70.0;
				break;

			default: //default (small)
				radius = 30.0;
				size = 1;

		}

		shape.setRadius(radius);

		//place circle so it is fully on the screen
		shape.setCenterX(radius);
		shape.setCenterY(radius);

		shape.setFill(Color.TRANSPARENT);
		shape.setStroke(Color.BLACK);

		//create a new label for this circle
		label = new Text("Default");
		label.setX(radius);
		label.setY(radius);

	}

	public void setConstName(String name)
	{
		constName = name;
	}

	public String getConstName()
	{
		return constName;
	}


	//moves the whole circle to a new postion

	public void move(double newX, double newY)
	{
		//first check if the circle is the bounds of the pane
		if (this.inBounds(newX, newY))
		{
			//move the circle itself
			shape.setCenterX(newX);
			shape.setCenterY(newY);

			//and its label
			label.setX(newX);
			label.setY(newY);
		}
	}

	//checks if the given position of the centre is the bounds of the cirle's pane
	private boolean inBounds(double x, double y)
	{

		//get the bounds of the parent Pane
		Bounds bounds = parentPane.getLayoutBounds();

		//calculate 2 furthest points on the circle and check if they are in bounds
		if (!bounds.contains(x + this.getRadiusPix(), y + this.getRadiusPix()))
		{
			return false;
		}

		if (!bounds.contains(x - this.getRadiusPix(), y - this.getRadiusPix()))
		{
			return false;
		}
		return true;
	}

	//returns the radius of the circle in pixels
	public double getRadiusPix()
	{
		return shape.getRadius();
	}

	public double getHDiameter()
	{
		return 2* getRadiusPix();
	}

	//a generic method of comparing the overlap of two shapes
	public boolean isOverlapping(LogicShape other)
	{
		//create a new shape which is the intersection of the two shapes
		Shape ints = Shape.intersect(this.getShape(), other.getShape());

		//temporarily display this shape to compute the area
		Group temp = (Group) this.getShape().getParent();
		temp.getChildren().add(ints);
		double area = ints.computeAreaInScreen();
		temp.getChildren().remove(ints);

		//if the area of the intersection is less than 1 pixel, the shapes don't overlap
		if (area < 10.0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	//checks if this circle and another is overlapping
	@Override
	public boolean isOverlapping(LogicCircle other)
	{
		//same method as generic version
		LogicShape upcast = other;
		return isOverlapping(upcast);
	}

	@Override
	public boolean isOverlapping(LogicSquare other)
	{
		//same method as generic version
		LogicShape upcast = other;
		return isOverlapping(upcast);
	}

	public boolean isHAligned(LogicCircle other)
	{
		Circle thisCircle = (Circle) this.getShape();
		Circle otherCircle = (Circle) other.getShape();

		if (thisCircle.getCenterX() == otherCircle.getCenterX())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean isHAligned(LogicSquare other)
	{
		Circle thisCircle = (Circle) this.getShape();
		double squareCentre = other.getX() + 0.5 * other.getLengthPix();

		if (thisCircle.getCenterX() == squareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}


	//returns true if this cirlce is to the left of the other
	@Override
	public boolean isLeftOf(LogicCircle other)
	{
		//we know both are circles, even if java doesn't
		Circle thisCircle = (Circle) this.getShape();
		Circle otherCircle = (Circle) other.getShape();

		if (thisCircle.getCenterX() < otherCircle.getCenterX())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isLeftOf(LogicSquare other)
	{
		//javafx rectangle does not have a getCentre() method, we have to calculate it ourself
		double squareCentre = other.getX() + 0.5 * other.getLengthPix();

		Circle thisCircle = (Circle) this.getShape();

		if (thisCircle.getCenterX() < squareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isRightOf(LogicCircle other)
	{
		Circle thisCircle = (Circle) this.getShape();
		Circle otherCircle = (Circle) other.getShape();

		if (thisCircle.getCenterX() > otherCircle.getCenterX())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isRightOf(LogicSquare other)
	{
		//javafx rectangle does not have a getCentre() method, we have to calculate it ourself
		double squareCentre = other.getX() + 0.5 * other.getLengthPix();

		Circle thisCircle = (Circle) this.getShape();

		if (thisCircle.getCenterX() > squareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	@Override
	public boolean isAbove(LogicCircle other)
	{
		Circle thisCircle = (Circle) this.getShape();
		Circle otherCircle = (Circle) other.getShape();

		//javafx treats 0 as the top row so 1 is above 2
		if (thisCircle.getCenterY() < otherCircle.getCenterY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isAbove(LogicSquare other)
	{
		//javafx rectangle does not have a getCentre() method, we have to calculate it ourself
		double squareCentre = other.getY() + 0.5 * other.getLengthPix();

		Circle thisCircle = (Circle) this.getShape();

		if (thisCircle.getCenterY() < squareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isBelow(LogicCircle other)
	{
		Circle thisCircle = (Circle) this.getShape();
		Circle otherCircle = (Circle) other.getShape();

		if (thisCircle.getCenterY() > otherCircle.getCenterY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isBelow(LogicSquare other)
	{
		//javafx rectangle does not have a getCentre() method, we have to calculate it ourself
		double squareCentre = other.getY() + 0.5 * other.getLengthPix();

		Circle thisCircle = (Circle) this.getShape();

		if (thisCircle.getCenterY() > squareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isBig()
	{
		if (this.size == 3)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isMed()
	{
		if (this.size == 2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isSmall()
	{
		if (this.size == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public double getX()
	{
		return this.shape.getTranslateX();
	}

	@Override
	public double getY()
	{
		return this.shape.getTranslateY();
	}

	@Override
	public Text getLabel()
	{
		return this.label;
	}

	//this seems kinda trivial but allows for nice polymorphism!
	@Override
	public boolean isCircle()
	{
		return true;
	}
	@Override
	public boolean isSquare()
	{
		return false;
	}

	@Override
	public void setLabel(String givenText)
	{
		this.label.setText(givenText);

	}

	@Override
	public Shape getShape()
	{
		return shape;
	}

	@Override
	public void setBold(boolean value)
	{
		if (value == true)
		{
			//set bold on
			this.shape.setStrokeWidth(5.0);
		}
		else
		{
			//turn bold off
			this.shape.setStrokeWidth(1.0);
		}
	}

	public ArrayList<Premise> testAllPredicates(LogicCircle other)
	{
		ArrayList<Premise> preds = new ArrayList<Premise>();

		//get the labels of the shapes for building premises
		String thisLabel = this.getLabel().getText();
		String otherLabel = other.getLabel().getText();

		Premise overlapping = new Premise(Predicate.IS_OVERLAPPING, thisLabel, otherLabel);
		Premise above = new Premise(Predicate.IS_ABOVE, thisLabel, otherLabel);
		Premise below = new Premise(Predicate.IS_BELOW, thisLabel, otherLabel);
		Premise left = new Premise(Predicate.IS_LEFT_OF, thisLabel, otherLabel);
		Premise right = new Premise(Predicate.IS_RIGHT_OF, thisLabel, otherLabel);

		if (this.isOverlapping(other))
		{
			preds.add(overlapping);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, overlapping));
		}

		if (this.isAbove(other))
		{
			preds.add(above);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, above));
		}

		if (this.isBelow(other))
		{
			preds.add(below);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, below));
		}

		if (this.isLeftOf(other))
		{
			preds.add(left);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, left));
		}

		if (this.isRightOf(other))
		{
			preds.add(right);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, right));
		}

		return preds;
	}
	public ArrayList<Premise> testAllPredicates(LogicSquare other)
	{
		ArrayList<Premise> preds = new ArrayList<Premise>();
		//get the labels of the shapes for building premises
		String thisLabel = this.getLabel().getText();
		String otherLabel = other.getLabel().getText();

		Premise overlapping = new Premise(Predicate.IS_OVERLAPPING, thisLabel, otherLabel);
		Premise above = new Premise(Predicate.IS_ABOVE, thisLabel, otherLabel);
		Premise below = new Premise(Predicate.IS_BELOW, thisLabel, otherLabel);
		Premise left = new Premise(Predicate.IS_LEFT_OF, thisLabel, otherLabel);
		Premise right = new Premise(Predicate.IS_RIGHT_OF, thisLabel, otherLabel);

		if (this.isOverlapping(other))
		{
			preds.add(overlapping);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, overlapping));
		}

		if (this.isAbove(other))
		{
			preds.add(above);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, above));
		}

		if (this.isBelow(other))
		{
			preds.add(below);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, below));
		}

		if (this.isLeftOf(other))
		{
			preds.add(left);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, left));
		}

		if (this.isRightOf(other))
		{
			preds.add(right);
		}
		else
		{
			preds.add(new Premise(Connective.NOT, right));
		}
		return preds;
	}

}
