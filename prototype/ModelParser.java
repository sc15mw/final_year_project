package prototype;
//reads in a mace4 model file and converts it into a set of shapes and predicates
import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

//takes a mace4 model file and converts it into a list of premises
public class ModelParser
{
	private String filepath; //where the model file is stored
	private BufferedReader modelIn; //the model file itself

	private ArrayList<Premise> premises; //the premises found

	private ArrayList<String> constants; //stores the constants
	private int noConsts = -1; //the number of constants in the model

	public ModelParser(String givenFilepath)
	{
		//set up file path and arrays
		filepath = givenFilepath;
		premises = new ArrayList<Premise>();
		constants = new ArrayList<String>();

		//open the model file
		try
		{
			modelIn = new BufferedReader(new FileReader(filepath));
		}
		catch (Exception e)
		{
			System.out.println("ERROR: Could not open model file at: " + filepath);
		}

	}

	//finds the constants of the model i.e. elements of the domain
	public ArrayList<Premise> parse()
	{

		try
		{
			String line = null;
			//skip fist line (contains nothing useful
			modelIn.readLine();

			//the next line gives the number of constants
			line = modelIn.readLine();
			findNoConsts(line);
			//create the constants
			createConstants(noConsts);


			//scan the file until a line containing the world "relation" is found
			while ((line = modelIn.readLine()) != null)
			{
				if (line.contains("relation"))
				{
					parseRelation(line);
				}
			}
		}
		catch (IOException e)
		{
			System.out.println("Error parsing file.");
		}

		return premises;

	}

	//finds the number of constants
	//the number of consts. is always given on line 2
	private void findNoConsts(String line)
	{
		//remove everything except the digits
		String temp = line.replaceAll("\\D+","");

		//convert to int and assign to field
		noConsts = Integer.parseInt(temp);

	}

	private void parseRelation(String line) throws IOException
	{
		//the string after the 3rd quotation marks is the predicate name
		//if we split the string using a quotation mark, this will be 4th string
		String[] temp = line.split("\"");
		String predicateName = temp[3];

		//splitting the line using commas gives the arity of the relation as the 3rd string
		String[] temp2 = line.split(",");
		//remove the space
		String temp3 = temp2[2].replace(" ", "");
		//convert to int
		int arity = Integer.parseInt(temp3);


		//convert the name of the predicate into a solid predicate object
		Predicate pred = Predicate.fromString(predicateName);

		//if the arity is 1, the truth values are all on the next line
		if (arity == 1)
		{
			String truthValues = modelIn.readLine();

			//check its not null and parse
			if (truthValues == null)
			{
				throw new IOException();
			}
			else
			{
				parseUnaryTruthValues(truthValues, pred);
			}
		}

		//if the arity is 2, the values are on the next "noConsts" lines
		if (arity == 2)
		{
			//skip one line
			modelIn.readLine();

			//read in all "noConst" lines
			String[] values = new String[noConsts];

			for (int i = 0; i < noConsts; i++)
			{
				values[i] = modelIn.readLine();

				if (values[i] == null)
				{
					throw new IOException();
				}
			}

			parseBinaryTruthValues(values, pred);

		}

		return;

	}

	//parses the truth values for the unary predicate given
	private void parseUnaryTruthValues(String values, Predicate pred)
	{
		//remove everything except digits
		values = values.replaceAll("\\D+","");

		//loop through the characters to get the individual truth values
		for (int i = 0; i < values.length(); i++)
		{
			//get the constant's name
			String constName = constants.get(i);

			char c = values.charAt(i);
			//c corresponds to the value of this predicate for constant i
			//e.g. if i = 3 and c = 1, then c3 has "true" for this predicate

			//create a premise
			Premise newPremise = new Premise(pred, constName);

			//if c = 0 i.e. predicate is false for this constant
			if (c == '0')
			{
				//we need to negate it
				newPremise = new Premise(Connective.NOT, newPremise);
			}

			//add the new premise
			premises.add(newPremise);

		}
	}

	//same as unary method but for binary predicates
	private void parseBinaryTruthValues(String[] values, Predicate pred)
	{
		System.out.println(pred.toString());
		//line i represents the values of constant i for this predicate
		//each value j on the line is the value for i with j
		//e.g. if i=1, j=0, c = 1 then pred(c1, c0) is true

		//remove everything except digits from all lines
		int i;
		for (i = 0; i < values.length; i++)
		{
			values[i] = values[i].replaceAll("\\D+","");
		}


		//parse each line
		for (i = 0; i < values.length; i++)
		{
			String firstArg = constants.get(i);

			//for each value in the line
			for (int j = 0; j < values[0].length(); j++)
			{
				//get second constant name
				String secondArg = constants.get(j);
				System.out.print("J = " + Integer.toString(j));
				//get truth value
				char c = values[i].charAt(j);

				System.out.println(secondArg + " = " + Character.toString(c));

				Premise newPremise = new Premise(pred, firstArg, secondArg);

				//if c = 0, pred(c0, cj) is false
				if (c == '0')
				{
					//negate the premise
					newPremise = new Premise(Connective.NOT, newPremise);
				}

				premises.add(newPremise);

			}
			System.out.println("---------------");
		}


		return;

	}

	//creates count no of constants and adds them to the constant array of this parser
	private void createConstants(int count)
	{
		for (int i = 0; i < count; i++)
		{
			String constantName = "c" + Integer.toString(i);
			constants.add(constantName);
		}
	}

	//returns the number of constants
	public int getNoConsts()
	{
		return noConsts;
	}
}
