package prototype;

public enum Connective
{
	//unary connectives
	NOT("-"),

	//binary
	AND("&"),
	OR("|"),
	IMPLIES("->");

	private final String text;

	private Connective(final String givenText)
	{
		this.text = givenText;
	}

	@Override
	public String toString()
	{
		return text;
	}

	public static Connective[] getAll()
	{
		Connective[] temp = new Connective[3];
		//temp[2] = Connective.NOT;
		temp[0] = Connective.AND;
		temp[1] = Connective.OR;
		temp[2] = Connective.IMPLIES;

		return temp;
	}
}