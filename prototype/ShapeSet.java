package prototype;

import java.util.ArrayList;

//describes a set of LogicShapes which are treated as a single shape
public class ShapeSet
{
	int noShapes = 0; //indicates the number of shapes in this set
	public ArrayList<LogicShape> shapes; //the shapes in the set

	public ShapeSet(ArrayList<LogicShape> givenShapes)
	{
		shapes = givenShapes;
		noShapes = shapes.size();
	}

	public ShapeSet(LogicShape givenShape)
	{
		shapes = new ArrayList<LogicShape>();
		shapes.add(givenShape);
		noShapes = 1;
	}

	public ShapeSet()
	{
		shapes = new ArrayList<LogicShape>();
	}

	public void addShape(LogicShape givenShape)
	{
		shapes.add(givenShape);
		noShapes++;
	}

	public int getNoShapes()
	{
		return noShapes;
	}

	//checks if the given shape is in this set
	public boolean contains(LogicShape givenShape)
	{
		for (LogicShape curShape: shapes)
		{
			if (givenShape == curShape)
			{
				return true;
			}
		}

		return false;
	}

	//moves every shape BY the given amount (otherwise, all shapes would overlap)
	public void moveBy(double dx, double dy)
	{
		//for each shape in the set
		for (LogicShape shape: shapes)
		{
			//get the current position of the shape
			double oldX = shape.getX();
			double oldY = shape.getY();

			//move the shape to the new position
			shape.move(oldX + dx, oldY + dy);
		}
	}

	//returns the amount of horizontal space this set takes up
	public double getHSpacing()
	{
		//the amount of space this set takes up is the same as the biggest shape in the set
		double biggestDiameter = 0;

		for (LogicShape shape: shapes)
		{
			if (shape.getHDiameter() > biggestDiameter)
			{
				biggestDiameter = shape.getHDiameter();
			}
		}

		return biggestDiameter;
	}

	//finds the shapes that are horizontally or vertically aligned given the world that contains them
	//combines them together into a shape set
	//wraps all other shapes in a shape set
	public static ArrayList<ShapeSet> clumpShapes(ArrayList<LogicShape> shapes, LogicWorld world, Orientation direction)
	{
		ArrayList<ShapeSet> shapeSets = new ArrayList<ShapeSet>();

		//consider every unique pair of constants and check if they are aligned
		for (int i = 0; i < shapes.size(); i++)
		{
			LogicShape first = shapes.get(i);
			boolean alignedFound = false; //shows if ANY shape is aligned with shape i

			for (int j = i + 1; j < shapes.size(); j++)
			{
				LogicShape second = shapes.get(j);

				boolean aligned = true;

				String iName = first.getConstName();
				String jName = second.getConstName();

				//create the hypotheses for the statements
				//if all these statements are false, the shapes are aligned
				Premise[] hypotheses = new Premise[4];

				//if we are clumping shapes horizontally
				if (direction == Orientation.HORIZONTAL)
				{
					hypotheses[0] = new Premise(Predicate.IS_LEFT_OF, iName, jName);
					hypotheses[1] = new Premise(Predicate.IS_LEFT_OF, jName, iName);
					hypotheses[2] = new Premise(Predicate.IS_RIGHT_OF, iName, jName);
					hypotheses[3] = new Premise(Predicate.IS_RIGHT_OF, jName, iName);
				}
				else if (direction == Orientation.VERTICAL)
				{
					//clumping vertically
					hypotheses[0] = new Premise(Predicate.IS_ABOVE, iName, jName);
					hypotheses[1] = new Premise(Predicate.IS_ABOVE, jName, iName);
					hypotheses[2] = new Premise(Predicate.IS_BELOW, iName, jName);
					hypotheses[3] = new Premise(Predicate.IS_BELOW, jName, iName);
				}


				//find all the binary premises that contain the two constants
				ArrayList<Premise> premises = world.getBinaryPremises(iName, jName);

				//check if any of them match a hypothesis
				for (Premise curPremise: premises)
				{
					for (int k = 0; k < 4; k++)
					{
						if (curPremise.PremEquals(hypotheses[k]))
						{
							aligned = false;
						}
					}
				}

				//if the shapes are aligned...
				if (aligned == true)
				{
					alignedFound = true;
					//place them in the same set
					ShapeSet set = new ShapeSet();
					set.addShape(first);
					set.addShape(second);

					shapeSets.add(set);
				}

			}

			//if we haven't found a shape that aligns with it yet...
			if (alignedFound == false)
			{
				//check if this shape hasn't already been placed in a set
				boolean placedInSet = false;
				for (ShapeSet curSet: shapeSets)
				{
					if (curSet.contains(first))
					{
						placedInSet = true;
					}
				}

				//if it is not already in a set, it must be on its own
				if (placedInSet == false)
				{
					//put it in a set on its own
					shapeSets.add(new ShapeSet(first));
				}

			}

		}

		return shapeSets;
	}


}
