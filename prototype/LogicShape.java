package prototype;
import javafx.scene.text.*;
import javafx.scene.shape.Shape;

import java.util.ArrayList;

public abstract class LogicShape
{
	protected String constName;

	//the predicates (each subclass must provide a method for comparing it with all other subclasses)
	abstract boolean isOverlapping(LogicCircle other);
	abstract boolean isOverlapping(LogicSquare other);

	abstract boolean isLeftOf(LogicCircle other);
	abstract boolean isLeftOf(LogicSquare other);

	abstract boolean isRightOf(LogicCircle other);
	abstract boolean isRightOf(LogicSquare other);

	abstract boolean isHAligned(LogicCircle other);
	abstract boolean isHAligned(LogicSquare other);

	abstract boolean isAbove(LogicCircle other);
	abstract boolean isAbove(LogicSquare other);

	abstract boolean isBelow(LogicCircle other);
	abstract boolean isBelow(LogicSquare other);

	//abstract boolean isInside(LogicCircle other);
	//abstract boolean isInside(LogicSquare other);

	abstract boolean isBig();
	abstract boolean isMed();
	abstract boolean isSmall();
	
	abstract boolean isCircle();
	abstract boolean isSquare();

	//often, we want to check all the predicates
	abstract ArrayList<Premise> testAllPredicates(LogicCircle other);
	abstract ArrayList<Premise> testAllPredicates(LogicSquare other);



	//methods for moving the visual shape around
	abstract void move(double x, double y);
	abstract void setBold(boolean toggle); //sets the bold on or off
	abstract double getX();
	abstract double getY();

	//the labels of the shape
	abstract Text getLabel();
	abstract void setLabel(String newLabel);

	//gets the reference to visual shape
	abstract Shape getShape();

	//gets the horizontal diameter of this shape
	abstract double getHDiameter();

	//sets or gets the constant name of the shape
	public abstract void setConstName(String name);
	public abstract String getConstName();
}
