package prototype;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.Arrays;


//display a given logic world in a new window
public class LogicWorldWindow
{
	Stage s;
	VBox root;

	ArrayList<LogicShape> onScreen; //keeps a list of all the shapes on screen
	LogicShape lastClicked; //the circle that was last clicked on
	Pane worldPane; //contains the shapes
	LogicWorld w; //the logic world currently being displayed

	public Group shapes;
	ArrayList<String> labels; //a collection of labels for the shapes


	public LogicWorldWindow()
	{
		lastClicked = null;
		onScreen = new ArrayList<LogicShape>();
		shapes = new Group();

		labels = new ArrayList<String>();
		String[] temp = new String[]{"Alpha", "Bravo", "Charlie", "Delta", "Echo"};
		labels.addAll(Arrays.asList(temp));

		worldPane = new Pane();
		worldPane.setPrefSize(550, 400);
		worldPane.getChildren().add(shapes);

		root = new VBox();
		root.getChildren().addAll(worldPane);

		s = new Stage();
		s.setScene(new Scene(root, 400, 300));
		s.setTitle("NO WORLD");

	}

	//returns the pane that this window displays
	public Pane getWorldPane()
	{
		return worldPane;
	}

	public void display()
	{
		s.show();
	}

	//displays the given shape on screen
	private void displayShape(LogicShape newShape)
	{
		//register the new shape with the action handlers
		//get the reference to the visual shape
		Shape newVisual = newShape.getShape();

		//display the shape
		shapes.getChildren().add(newVisual);

		//add to list of shapes
		onScreen.add(newShape);

		//set the label
		newShape.setLabel(getLabel());
		//display the label
		worldPane.getChildren().add(newShape.getLabel());

		//move the shape away from the edge
		newShape.move(newShape.getX() + 100, newShape.getY()+ 100);

	}

	//gets a unique label for a shape
	private String getLabel()
	{
		//check if the list of available labels is empty
		if (labels.isEmpty())
		{
			//if there are no labels left, use a number
			return Integer.toString(onScreen.size());
		}
		else
		{
			//remove a label and return this
			return labels.remove(0);
		}
	}

	//displays the given LogicWorld object in the ui
	//i.e. displays all the shapes such that the premises of the world hold
	public void setLogicWorld(LogicWorld world)
	{
		//set the title of the window to the world's name
		s.setTitle("WORLD: " + world.worldName);

		//get the list of shapes from the world
		ArrayList<LogicShape> worldShapes = world.getShapes();

		//display all the shapes
		for (LogicShape curShape: worldShapes)
		{
			displayShape(curShape);
		}

		//we should now have created all the shapes with the correct size
		//we must now satisfy the binary predicates

		//get the horizontal ordering of the shapes
		ShapeSet[] HorOrder = world.getHOrder();
		//now we have to horizontal order of the shapes, position them in this order
		positionShapes(HorOrder, Orientation.HORIZONTAL);

		//now order the shapes vertically
		ShapeSet[] VerOrder = world.getVOrder();
		positionShapes(VerOrder, Orientation.VERTICAL);

	}

	//moves the shapes so that their order matches the order in the array
	private void positionShapes(ShapeSet[] sets, Orientation direction)
	{
		//all shapes are intially at the origin
		//leave the first set where it is

		int noSets = sets.length;
		double offset = 0.0;

		for (int i = 1; i < noSets; i++)
		{
			//add to the offset to the previous shapes radius + a little bit of spacing
			offset += sets[i-1].getHSpacing() + 5.0;

			//move the set to that position
			if (direction == Orientation.HORIZONTAL)
			{
				sets[i].moveBy(offset, 0.0);
			}
			else if (direction == Orientation.VERTICAL)
			{
				sets[i].moveBy(0.0, offset);
			}

		}
	}


}
