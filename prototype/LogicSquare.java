package prototype;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.*;
import javafx.scene.layout.Pane;
import javafx.scene.Group;
import javafx.scene.paint.Color;

import javafx.scene.shape.Circle;

import javafx.geometry.Bounds;

import java.util.ArrayList;

public class LogicSquare extends LogicShape
{
		//FIELDS
	Rectangle shape;
	private int size; //holds the size of this square
									 //NOT actual size but (1 = small, 2 = medium, 3 = big)

	private Pane parentPane; //the pane that is displaying this square
	public Text label; //reference to the label of this square


	//CONSTRUCTOR
	public LogicSquare(Pane givenParent, int givenSize)
	{
		//create a default square using javafx
		shape = new Rectangle();

		//set the parent
		parentPane = givenParent;

		//select the size
		size = givenSize;

		double length = 0.0;
		switch(givenSize)
		{
			case 1: //small square
				length = 40.0;
				break;
			case 2: //medium
				length = 80.0;
				break;
			case 3: //big
				length = 120.0;

		}

		shape.setWidth(length);
		shape.setHeight(length);

		shape.setFill(Color.TRANSPARENT);
		shape.setStroke(Color.RED);

		//create a new label for this circle
		label = new Text("Default");
		label.setFill(Color.RED);

		//place the label at the centre of the square
		label.setX(length/2);
		label.setY(length/2);

	}

	public void setConstName(String name)
	{
		constName = name;
	}

	public String getConstName()
	{
		return constName;
	}

	public double getLengthPix()
	{
		return shape.getHeight();
	}

	public double getHDiameter()
	{
		return 2*getLengthPix();
	}

	//moves the whole square to a new postion
	@Override
	public void move(double newX, double newY)
	{
		//if the new position is in bounds...
		if (inBounds(newX, newY))
		{
			//move the square
			shape.setX(newX);
			shape.setY(newY);

			//the label still needs to be at the centre of the square
			label.setX(newX + 0.5 * this.getLengthPix());
			label.setY(newY + 0.5 * this.getLengthPix());
		}
	}

	//checks that if the square is in bounds if moved to x,y
	private boolean inBounds(double x, double y)
	{
		//Circle's position is defined by its centre point.
		//Rectangle however is defined by its top left corner

		Bounds parentBounds = parentPane.getLayoutBounds();

		//check the square does not move above the top or left
		if (x < 0 | y < 0)
		{
			return false;
		}

		//check it does not move off the bottom
		if (!parentBounds.contains(x + this.getLengthPix(), y + this.getLengthPix()))
		{
			return false;
		}

		return true;
	}

	//a generic method of comparing the overlap of two shapes
	public boolean isOverlapping(LogicShape other)
	{
		//create a new shape which is the intersection of the two shapes
		Shape ints = Shape.intersect(this.getShape(), other.getShape());

		//temporarily display this shape to compute the area
		Group temp = (Group) this.getShape().getParent();
		temp.getChildren().add(ints);
		double area = ints.computeAreaInScreen();
		temp.getChildren().remove(ints);

		//if the area of the intersection is less than 1 pixel, the shapes don't overlap
		if (area < 1.0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	@Override
	public boolean isOverlapping(LogicCircle other)
	{
		//same method as generic version
		LogicShape upcast = other;
		return isOverlapping(upcast);
	}

	@Override
	public boolean isOverlapping(LogicSquare other)
	{
		//same method as generic version
		LogicShape upcast = other;
		return isOverlapping(upcast);
	}


	public boolean isHAligned(LogicCircle other)
	{
		double squareCentre = this.getX() + 0.5 * this.getLengthPix();
		Circle otherCircle = (Circle) other.getShape();

		if (squareCentre == otherCircle.getCenterX())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean isHAligned(LogicSquare other)
	{
		double thisSquareCentre = this.getX() + 0.5 * this.getLengthPix();
		double otherSquareCentre = other.getX() + 0.5 * this.getLengthPix();

		if (thisSquareCentre == otherSquareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	@Override
	public boolean isLeftOf(LogicCircle other)
	{
		//we have to calculate the centre of this square ourselves
		double thisSquareCentre = this.getX() + 0.5 * this.getLengthPix();
		Circle otherCircle = (Circle) other.getShape();

		if (thisSquareCentre < otherCircle.getCenterX())
		{
			return true;
		}
		else
		{
			return false;
		}

	}

		@Override
	public boolean isLeftOf(LogicSquare other)
	{
		double thisSquareCentre = this.getX() + 0.5 * this.getLengthPix();
		double otherSquareCentre = other.getX() + 0.5 * this.getLengthPix();

		if (thisSquareCentre < otherSquareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isRightOf(LogicCircle other)
	{
		//we have to calculate the centre of this square ourselves
		double thisSquareCentre = this.getX() + 0.5 * this.getLengthPix();
		Circle otherCircle = (Circle) other.getShape();

		if (thisSquareCentre > otherCircle.getCenterX())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

		@Override
	public boolean isRightOf(LogicSquare other)
	{
		double thisSquareCentre = this.getX() + 0.5 * this.getLengthPix();
		double otherSquareCentre = other.getX() + 0.5 * this.getLengthPix();

		if (thisSquareCentre > otherSquareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isAbove(LogicCircle other)
	{
		Circle otherCircle = (Circle) other.getShape();

		double thisSquareCentre = this.getY() + 0.5 * this.getLengthPix();

		//javafx treats 0 as the top row so 1 is above 2
		if (thisSquareCentre < otherCircle.getCenterY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isAbove(LogicSquare other)
	{
		double thisSquareCentre = this.getY() + 0.5 * this.getLengthPix();
		double otherSquareCentre = other.getY() + 0.5 * other.getLengthPix();

		//javafx treats 0 as the top row so 1 is above 2
		if (thisSquareCentre < otherSquareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isBelow(LogicCircle other)
	{
		Circle otherCircle = (Circle) other.getShape();

		double thisSquareCentre = this.getY() + 0.5 * this.getLengthPix();

		//javafx treats 0 as the top row so 1 is above 2
		if (thisSquareCentre > otherCircle.getCenterY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isBelow(LogicSquare other)
	{
		double thisSquareCentre = this.getY() + 0.5 * this.getLengthPix();
		double otherSquareCentre = other.getY() + 0.5 * other.getLengthPix();

		//javafx treats 0 as the top row so 1 is above 2
		if (thisSquareCentre > otherSquareCentre)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isBig()
	{
		if (this.size == 3)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isMed()
	{
		if (this.size == 2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isSmall()
	{
		if (this.size == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//this seems kinda trivial but allows for nice polymorphism!
	@Override
	public boolean isCircle()
	{
		return false;
	}
	@Override
	public boolean isSquare()
	{
		return true;
	}


	@Override
	public double getX()
	{
		return this.shape.getX();
	}

	@Override
	public double getY()
	{
		return this.shape.getY();
	}

	@Override
	public Text getLabel()
	{
		return this.label;
	}

	@Override
	public void setBold(boolean value)
	{
		if (value == true)
		{
			//set bold on
			this.shape.setStrokeWidth(5.0);
			this.label.setStrokeWidth(5.0);
		}
		else
		{
			//turn bold off
			this.shape.setStrokeWidth(1.0);
			this.label.setStrokeWidth(1.0);
		}
	}

	@Override
	public void setLabel(String givenText)
	{
		this.label.setText(givenText);
	}

	@Override
	public Shape getShape()
	{
		return shape;
	}

	public ArrayList<Premise> testAllPredicates(LogicCircle other)
	{
		ArrayList<Premise> preds = new ArrayList<Premise>();
		//get the labels of the shapes for building premises
		String thisLabel = this.getLabel().getText();
		String otherLabel = other.getLabel().getText();

		if (this.isOverlapping(other))
		{
			preds.add(new Premise(Predicate.IS_OVERLAPPING, thisLabel, otherLabel));
		}

		if (this.isAbove(other))
		{
			preds.add(new Premise(Predicate.IS_ABOVE, thisLabel, otherLabel));
		}

		if (this.isBelow(other))
		{
			preds.add(new Premise(Predicate.IS_BELOW, thisLabel, otherLabel));
		}

		if (this.isLeftOf(other))
		{
			preds.add(new Premise(Predicate.IS_LEFT_OF, thisLabel, otherLabel));
		}

		if (this.isRightOf(other))
		{
			preds.add(new Premise(Predicate.IS_RIGHT_OF, thisLabel, otherLabel));
		}

		if (this.isHAligned(other))
		{
			preds.add(new Premise(Predicate.IS_H_ALIGNED, thisLabel, otherLabel));
		}
		return preds;
	}

	public ArrayList<Premise> testAllPredicates(LogicSquare other)
	{
		ArrayList<Premise> preds = new ArrayList<Premise>();
		//get the labels of the shapes for building premises
		String thisLabel = this.getLabel().getText();
		String otherLabel = other.getLabel().getText();

		if (this.isOverlapping(other))
		{
			preds.add(new Premise(Predicate.IS_OVERLAPPING, thisLabel, otherLabel));
		}

		if (this.isAbove(other))
		{
			preds.add(new Premise(Predicate.IS_ABOVE, thisLabel, otherLabel));
		}

		if (this.isBelow(other))
		{
			preds.add(new Premise(Predicate.IS_BELOW, thisLabel, otherLabel));
		}

		if (this.isLeftOf(other))
		{
			preds.add(new Premise(Predicate.IS_LEFT_OF, thisLabel, otherLabel));
		}

		if (this.isRightOf(other))
		{
			preds.add(new Premise(Predicate.IS_RIGHT_OF, thisLabel, otherLabel));
		}

		if (this.isHAligned(other))
		{
			preds.add(new Premise(Predicate.IS_H_ALIGNED, thisLabel, otherLabel));
		}
		return preds;
	}
}
