package prototype;

import javafx.application.Application;

import javafx.event.EventHandler;
import javafx.event.ActionEvent;

import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.text.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Shape;
import javafx.scene.layout.*;
import javafx.scene.control.Accordion;

import javafx.stage.Stage;
import javafx.stage.Modality;

import java.util.ArrayList;
import java.util.Arrays;


public class MainApp extends Application
{
	ArrayList<LogicShape> onScreen; //keeps a list of all the shapes on screen
	LogicShape lastClicked; //the circle that was last clicked on

	VBox root;

	Pane worldPane; //contains the shapes
	Accordion shapeSel;
	HBox worldLayout; //holds the world pane and the shape selector

	HBox controlPane; //contains buttons

	HBox inputPane; //contains the input
	TextField inputBox;

	HBox resultPane; //contains the results
	Text resultBox; //shows if the statement typed by the user is true or not

	Stage dialog;
	TextField newLabelBox; //used in the shape editing dialogue
	Text resultMsg;

	CheckBox trueWorld;
	CheckBox falseWorld; //indicates if we should generate a world where the statement is false too

	LogicWorld w; //the logic world currently being displayed

	public Group shapes;
	double startX; //used when calculating how to move a shape
	double startY;

	public int timeLimit = 30; //the time limit in seconds for any prover9/mace4 operation
	public boolean allowMultLabels = false;

	ArrayList<String> labels; //a collection of labels for the shapes

	@Override
	public void start(Stage s)
	{
		lastClicked = null;
		onScreen = new ArrayList<LogicShape>();
		shapes = new Group();

		w = new LogicWorld("User's world", null, timeLimit);

		labels = new ArrayList<String>();
		String[] temp = new String[]{"Alpha", "Bravo", "Charlie", "Delta", "Echo"};
		labels.addAll(Arrays.asList(temp));

		worldPane = new Pane();
		worldPane.setPrefSize(550, 400);
		worldPane.getChildren().add(shapes);

		MenuBar menuBar = new MenuBar();

		Menu help = new Menu("Help");
		MenuItem howtouseHelp = new MenuItem("How to use");
		howtouseHelp.setOnAction(openHelp);
		MenuItem listPredicates = new MenuItem("List of available predicates");
		help.getItems().addAll(howtouseHelp, listPredicates);

		Menu options = new Menu("Options");
		MenuItem editOptionsItem = new MenuItem("Edit options");
		editOptionsItem.setOnAction(editOptions);
		options.getItems().addAll(editOptionsItem);

		menuBar.getMenus().addAll(help, options);

		Button largeCircleBtn = new Button("Big");
		largeCircleBtn.setOnAction(addLargeCircle);

		Button medCircleBtn = new Button("Med");
		medCircleBtn.setOnAction(addMedCircle);

		Button smallCircleBtn = new Button("Small");
		smallCircleBtn.setOnAction(addSmallCircle);

		Button largeSquareBtn = new Button("Big");
		largeSquareBtn.setOnAction(addLargeSquare);

		Button medSquareBtn = new Button("Med");
		medSquareBtn.setOnAction(addMedSquare);

		Button smallSquareBtn = new Button("Small");
		smallSquareBtn.setOnAction(addSmallSquare);

		Button testBtn = new Button("Test");
		testBtn.setOnAction(test);

		Button clearBtn = new Button("Delete");
		clearBtn.setOnAction(delete);

		Button deleteAllBtn = new Button("Delete All");
		deleteAllBtn.setOnAction(deleteAll);

		shapeSel = new Accordion();

		TitledPane circleSel = new TitledPane();
		VBox circleSelLayout = new VBox();
		circleSel.setText("Circles");
		circleSelLayout.getChildren().addAll(largeCircleBtn, medCircleBtn, smallCircleBtn);
		circleSel.setContent(circleSelLayout);

		TitledPane squareSel = new TitledPane();
		VBox squareSelLayout = new VBox();
		squareSel.setText("Squares");
		squareSelLayout.getChildren().addAll(largeSquareBtn, medSquareBtn, smallSquareBtn);
		squareSel.setContent(squareSelLayout);

		shapeSel.getPanes().addAll(circleSel, squareSel);
		shapeSel.setExpandedPane(circleSel);

		worldLayout = new HBox();
		worldLayout.getChildren().addAll(worldPane, shapeSel);

		controlPane = new HBox();
		controlPane.setSpacing(5);
		controlPane.getChildren().addAll(clearBtn, deleteAllBtn);

		inputBox = new TextField("Enter the statement to test here");
		inputBox.setPrefWidth(400);

		Button clearTxtBoxBtn = new Button("Clear");
		clearTxtBoxBtn.setOnAction(clearTxtbox);

		Button createWorldBtn = new Button("Create World");
		createWorldBtn.setOnAction(createWorld);

		trueWorld = new CheckBox("Create True world");
		trueWorld.setSelected(true);
		falseWorld = new CheckBox("Create False world");

		VBox selectWorld = new VBox();
		selectWorld.getChildren().addAll(trueWorld, falseWorld);

		inputPane = new HBox();
		inputPane.setSpacing(5);
		inputPane.getChildren().addAll(inputBox, testBtn, clearTxtBoxBtn, createWorldBtn, selectWorld);

		resultBox = new Text();

		resultPane = new HBox();
		resultPane.setSpacing(5);
		resultPane.getChildren().add(resultBox);

		Button isBig = new Button("isBig");
		isBig.setOnAction(isBigClick);
		Button isMed = new Button("isMed");
		isMed.setOnAction(isMedClick);
		Button isSmall = new Button("isSmall");
		isSmall.setOnAction(isSmallClick);
		Button isCircle = new Button("isCircle");
		isCircle.setOnAction(isCircleClick);
		Button isSquare = new Button("isSquare");
		isSquare.setOnAction(isSquareClick);

		Accordion predSel = new Accordion();

		TitledPane unSel = new TitledPane();
		HBox unSelLayout = new HBox();
		unSel.setText("Unary Predicates");
		unSelLayout.getChildren().addAll(isBig, isMed, isSmall, isCircle, isSquare);
		unSel.setContent(unSelLayout);

		Button isLeftOf = new Button("isLeftOf");
		isLeftOf.setOnAction(isLeftOfClick);
		Button isRightOf = new Button("isRightOf");
		isRightOf.setOnAction(isRightOfClick);
		Button isAbove = new Button("isAbove");
		isAbove.setOnAction(isAboveClick);
		Button isBelow = new Button("isBelow");
		isBelow.setOnAction(isBelowClick);
		Button isOverlapping = new Button("isOverlapping");
		isOverlapping.setOnAction(isOverlappingClick);

		TitledPane binSel = new TitledPane();
		HBox binSelLayout = new HBox();
		binSel.setText("Binary Predicates");
		binSelLayout.getChildren().addAll(isLeftOf, isRightOf, isAbove, isBelow, isOverlapping);
		binSel.setContent(binSelLayout);

		Button forAll = new Button("all (Universal)");
		forAll.setOnAction(forAllClick);
		Button thereExists = new Button("exists (Existential)");
		thereExists.setOnAction(existsClick);
		Button and = new Button("& (and)");
		and.setOnAction(andClick);
		Button or = new Button("| (or)");
		or.setOnAction(orClick);
		Button implies = new Button("-> (implies)");
		implies.setOnAction(impliesClick);
		Button not = new Button("- (not)");
		not.setOnAction(notClick);



		TitledPane quantSel = new TitledPane();
		HBox quantSelLayout = new HBox();
		quantSel.setText("Quantifier & Connectives");
		quantSelLayout.getChildren().addAll(forAll, thereExists, and, or, implies, not);
		quantSel.setContent(quantSelLayout);


		predSel.getPanes().addAll(unSel, binSel, quantSel);
		predSel.setExpandedPane(unSel);

		root = new VBox();
		root.setSpacing(5);
		root.getChildren().addAll(menuBar, worldLayout, controlPane, inputPane, resultPane, predSel);


		s.setScene(new Scene(root, 800, 600));

		s.show();

	}

	EventHandler<ActionEvent> editOptions = new EventHandler<ActionEvent>()
	{
		@Override
		public void handle(ActionEvent event)
		{
			//open an options window
			openOptionsWindow();
		}
	};

	private void openOptionsWindow()
	{
		OptionsWindow newWindow = new OptionsWindow(this);
		newWindow.display();
	}

	EventHandler<ActionEvent> openHelp = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			HelpWindow newWindow = new HelpWindow();
			newWindow.display();
		}
	};


	EventHandler<MouseEvent> click = new EventHandler<MouseEvent> ()
	{
		@Override
		public void handle(MouseEvent e)
		{
			//remove the highlight from the previously clicked object (if there is one)
			if (lastClicked != null)
			{
				lastClicked.setBold(false);
			}

			//find the source of the event
			Shape source = (Shape) e.getSource();

			//find the logicshape this shape corresponds to
			LogicShape logicSource = findLogicShape(source);

			//remember that this shape was the latest to be clicked
			lastClicked = logicSource;

			//add a highlight to the shape
			logicSource.setBold(true);

			//if the user double clicked, they want to change the label
			if (e.getClickCount() == 2)
			{
				displayEditLabel(logicSource);
			}

		}
	};


	//when a text box is clicked
	EventHandler<ActionEvent> clearTxtbox = new EventHandler<ActionEvent> ()
	{
		@Override
		public void handle(ActionEvent e)
		{
			inputBox.setText("");

		}
	};

	EventHandler<ActionEvent> createWorld = new EventHandler<ActionEvent>()
	{
		@Override
		public void handle(ActionEvent e)
		{
			//reset result text
			resultBox.setText("");

			//get the statement from the textbox
			String input = inputBox.getText();

			//check that the input is valid prolog

			//convert to premise
			Premise conc = new Premise(input);
			ArrayList<Premise> newWorldPremises = new ArrayList<Premise>();
			newWorldPremises.add(conc);

			boolean worldPaneOccupied = false; //indicates if the world pane is already displaying a world

			//get the world that satisfies the premise
			if (trueWorld.isSelected())
			{
				try
				{
					resultBox.setText("Finding \"true\" world...");
					w = new LogicWorld(input,worldPane, newWorldPremises, timeLimit);
					displayLogicWorld(w);
					worldPaneOccupied = true;
					resultBox.setText("True World found.");
				}
				catch (ModelException ex)
				{
					//a model could not be found
					resultBox.setText("True world not found");
				}

			}

			if (falseWorld.isSelected())
			{
				//negate the conclusion and find a world in which it holds
				try
				{
					Premise negated = new Premise(Connective.NOT, conc);
					ArrayList<Premise> neg = new ArrayList<Premise>();
					neg.add(negated);

					String oldMessage = resultBox.getText();

					resultBox.setText("Finding \"false\" world...");


					//if the world pane is already occupied create a new window
					if (worldPaneOccupied == true)
					{
						LogicWorldWindow newWindow = new LogicWorldWindow();
						Pane newWorldPane = newWindow.getWorldPane();
						LogicWorld falseWorld = new LogicWorld(negated.toString(), newWorldPane, neg, timeLimit);
						newWindow.setLogicWorld(falseWorld);
						newWindow.display();
					}
					else
					{
						//else display it in the main window's world pane
						LogicWorld falseWorld = new LogicWorld(negated.toString(), worldPane, neg, timeLimit);
						displayLogicWorld(falseWorld);
					}

					resultBox.setText(oldMessage + " False world found.");


				}
				catch (ModelException ex2)
				{
					//append the result to the result box
					resultBox.setText(resultBox.getText() + " False world not found.");
				}
			}
		}
	};


		EventHandler<ActionEvent> test = new EventHandler<ActionEvent> ()
	{
		@Override
		public void handle(ActionEvent e)
		{
			String conc = inputBox.getText();
			resultBox.setText("");

			//take any parts with ( ... = ...) or (... != ...)
			//and replace them with true or false

			//check the syntax of the entered text

				extractPremises();
				boolean result = w.logicallyEntails(new Premise(conc));

				if (result == true)
				{
					resultBox.setText("True (Proof found)");
				}
				else
				{
					resultBox.setText("False (No proof found)");
				}


		}
	};


	EventHandler<MouseEvent> drag = new EventHandler<MouseEvent> ()
	{
		@Override
		public void handle(MouseEvent e)
		{
			//find the source of the event
			Shape source = (Shape) e.getSource();

			//get the position of the source of the event i.e. where the user stopped dragging
			double endX = e.getSceneX();
			double endY = e.getSceneY();

			//find the logic shape the source is part of
			LogicShape logicSource = findLogicShape(source);

			//move the source here
			logicSource.move(endX, endY);
		}
	};

 EventHandler<ActionEvent> addLargeCircle = new EventHandler<ActionEvent>() {

   @Override
   public void handle(ActionEvent e) {

   	//create a new circle
    LogicShape newCircle = new LogicCircle(worldPane, 3);

  	displayShape(newCircle);
   }
};

 EventHandler<ActionEvent> addMedCircle = new EventHandler<ActionEvent>() {

   @Override
   public void handle(ActionEvent e) {

   	//create a new circle
    LogicShape newCircle = new LogicCircle(worldPane, 2);
    displayShape(newCircle);

   }
};

 EventHandler<ActionEvent> addSmallCircle = new EventHandler<ActionEvent>() {

   @Override
   public void handle(ActionEvent e) {

   	//create a new circle
    LogicShape newCircle = new LogicCircle(worldPane, 1);
    displayShape(newCircle);

   }
};

 EventHandler<ActionEvent> addLargeSquare = new EventHandler<ActionEvent>() {

   @Override
   public void handle(ActionEvent e) {

   	//create a new square
    LogicShape newSquare = new LogicSquare(worldPane, 3);

  	displayShape(newSquare);

   }
};

 EventHandler<ActionEvent> addMedSquare = new EventHandler<ActionEvent>() {

   @Override
   public void handle(ActionEvent e) {

   	//create a new square
    LogicShape newSquare = new LogicSquare(worldPane, 2);

  	displayShape(newSquare);

   }
};

 EventHandler<ActionEvent> addSmallSquare = new EventHandler<ActionEvent>() {

   @Override
   public void handle(ActionEvent e) {

   	//create a new square
    LogicShape newSquare = new LogicSquare(worldPane, 1);

  	displayShape(newSquare);

   }
};

EventHandler<ActionEvent> deleteAll = new EventHandler<ActionEvent>() {

	@Override
	public void handle(ActionEvent e) {

	 deleteAllShapes();


	}
};

 EventHandler<ActionEvent> delete = new EventHandler<ActionEvent>() {

   @Override
   public void handle(ActionEvent e) {

   	//delete the last thing the user clicked on, if they clicked something
   	if (lastClicked != null)
   	{
   		deleteShape(lastClicked);
   	}

   }
};
   EventHandler<ActionEvent> edit = new EventHandler<ActionEvent>() {

   @Override
   public void handle(ActionEvent e) {

   	//if the text box for entering the label and the dialogue box are open...
  	if (newLabelBox != null && dialog != null)
  	{
  		//get the users input and the old label
  		String newLabel = newLabelBox.getText();
		String oldLabel = lastClicked.getLabel().getText();

		//check the new label isn't a blank string
		if (newLabel.equals(""))
		{
			resultMsg.setText("Label cannot be empty");
			return;
		}

		//check if the label is already taken
		boolean labelTaken = false;
		for (LogicShape shape: onScreen)
		{
			if (shape.getLabel().getText().equals(newLabel))
			{
				labelTaken = true;
			}
		}

		//if the label is already taken, and it is not the same as the old, and we don't allow multiple labels
		if (labelTaken && !newLabel.equals(oldLabel) && !allowMultLabels)
		{
			resultMsg.setText("Label already in use");
		}
		else
		{
			//change the label
			lastClicked.setLabel(newLabel);
			lastClicked.setConstName(newLabel);
			//close the dialogue box
			dialog.close();
		}
  	}

   }

};

	EventHandler<ActionEvent> isBigClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isBig(*)");
		}

	};

	EventHandler<ActionEvent> isMedClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isMed(*)");
		}

	};

	EventHandler<ActionEvent> isSmallClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isSmall(*)");
		}

	};

	EventHandler<ActionEvent> isCircleClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isCircle(*)");
		}

	};

	EventHandler<ActionEvent> isSquareClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isSquare(*)");
		}

	};

	EventHandler<ActionEvent> isLeftOfClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isLeftOf(*,*)");
		}

	};

	EventHandler<ActionEvent> isRightOfClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isRightOf(*,*)");
		}

	};

	EventHandler<ActionEvent> isAboveClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isAbove(*,*)");
		}

	};

	EventHandler<ActionEvent> isBelowClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isBelow(*,*)");
		}

	};

	EventHandler<ActionEvent> isOverlappingClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("isOverlapping(*,*)");
		}

	};

	EventHandler<ActionEvent> forAllClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("all");
		}

	};

	EventHandler<ActionEvent> existsClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("exists");
		}

	};

	EventHandler<ActionEvent> andClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("&");
		}

	};

	EventHandler<ActionEvent> orClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("|");
		}

	};

	EventHandler<ActionEvent> impliesClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("->");
		}

	};

	EventHandler<ActionEvent> notClick = new EventHandler<ActionEvent>() {

		@Override
		public void handle(ActionEvent e) {
			appendToInputBox("-");
		}

	};

	//append the given string to the input box
	public void appendToInputBox(String toAppend)
	{
		//get what the user has typed
		String userTyped = inputBox.getText();

		//set the box to the result
		inputBox.setText(userTyped + toAppend);
	}

	//finds all the premises of the world and adds them to the LogicWorld
	public void extractPremises()
	{
		//remove all existing premises
		w.clearPremises();

		//for each shape on screen
		for (LogicShape cur: onScreen)
		{
			String curLabel = cur.getLabel().getText();

			//calculate the unary predicates for this shape
			Premise big = new Premise(Predicate.IS_BIG, curLabel);
			Premise med = new Premise(Predicate.IS_MED, curLabel);
			Premise small = new Premise(Predicate.IS_SMALL, curLabel);

			Premise circle = new Premise(Predicate.IS_CIRCLE, curLabel);
			Premise square = new Premise(Predicate.IS_SQUARE, curLabel);

			//check which hold, and add negations for all that don't hold
			if (cur.isBig())
			{
				w.addPremise(big);
				w.addPremise(new Premise(Connective.NOT, med));
				w.addPremise(new Premise(Connective.NOT, small));
			}
			else if (cur.isMed())
			{
				w.addPremise(med);
				w.addPremise(new Premise(Connective.NOT, big));
				w.addPremise(new Premise(Connective.NOT, small));
			}
			else if (cur.isSmall())
			{
				w.addPremise(small);
				w.addPremise(new Premise(Connective.NOT, big));
				w.addPremise(new Premise(Connective.NOT, med));
			}

			if (cur.isCircle())
			{
				w.addPremise(circle);
				w.addPremise(new Premise(Connective.NOT, square));
			}
			else if (cur.isSquare())
			{
				w.addPremise(square);
				w.addPremise(new Premise(Connective.NOT, circle));
			}

			//compare it with every other shape on screen for binary predicates
			for (LogicShape other: onScreen)
			{

				//we don't want to compare the shape with itself
				if (cur == other)
				{
					continue;
				}

				if (other instanceof LogicCircle)
				{
					//test all the binary predicates with the other shape
					ArrayList<Premise> temp = cur.testAllPredicates((LogicCircle) other);
					//add them to the world
					w.addPremises(temp);
				}
				else if (other instanceof LogicSquare)
				{
					//test all the binary predicates with the other shape
					ArrayList<Premise> temp = cur.testAllPredicates((LogicSquare) other);
					//add them to the world
					w.addPremises(temp);
				}

			}
		}

	}

	//gets a unique label for a shape
	private String getLabel()
	{
		//check if the list of available labels is empty
		if (labels.isEmpty())
		{
			//if there are no labels left, use a number
			return Integer.toString(onScreen.size());
		}
		else
		{
			//remove a label and return this
			return labels.remove(0);
		}
	}

	//handles the dialogue window for changing a shapes label
	private void displayEditLabel(LogicShape givenShape)
	{
		//set up the dialog box
		dialog = new Stage();
		dialog.initModality(Modality.APPLICATION_MODAL);
		VBox root = new VBox();

		String msgString = "Enter new label for: " + givenShape.getLabel().getText();
		Text msg = new Text(msgString);


		newLabelBox = new TextField();

		HBox bottomButtons = new HBox();
		Button changeBtn = new Button("Change");
		changeBtn.setOnAction(edit);

		//the cancel button simply closes the window
		Button cancelBtn = new Button("Cancel");
		cancelBtn.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent event)
			{
				dialog.close();
			}
		});

		bottomButtons.getChildren().addAll(changeBtn, cancelBtn);

		//used later to display if the changing of the label was successful
		resultMsg = new Text("");

		root.getChildren().addAll(msg, newLabelBox, bottomButtons, resultMsg);
		Scene newScene = new Scene(root, 300, 200);
		dialog.setScene(newScene);
		dialog.show();
	}

	//displays the given shape on screen
	private void displayShape(LogicShape newShape)
	{
		//register the new shape with the action handlers
		//get the reference to the visual shape
		Shape newVisual = newShape.getShape();
		newVisual.setOnMousePressed(click);
		newVisual.setOnMouseDragged(drag);

    	//display the shape
		shapes.getChildren().add(newVisual);

		//add to list of shapes
		onScreen.add(newShape);

		//add to logicworld
		w.addShape(newShape);

		//set the label
		String name = getLabel();
		newShape.setLabel(name);
		newShape.setConstName(name);


		//display the label
		worldPane.getChildren().add(newShape.getLabel());

		//move the shape away from the edge
		newShape.move(newShape.getX() + 100, newShape.getY()+ 100);

	}

	private void deleteShape(LogicShape shape)
	{
		//get the label of the shape
		String labelText = shape.getLabel().getText();

		//add the label back to the available pool of labels
		labels.add(labelText);

		//remove the label of the shape
 		shape.setLabel("");
 		//find the last clicked shape and delete it
 		shapes.getChildren().remove(shape.getShape());
 		//remove it from the array of onscreen shapes
 		onScreen.remove(shape);
 		w.deleteShape(shape);
	}

	private void deleteAllShapes()
	{
		int onScreenSize = onScreen.size();
		for (int i = 0; i < onScreenSize; i++)
		{
			deleteShape(onScreen.get(0));
		}

		w.clearShapes();
	}


	//finds the logic shape that a given shape corresponds to
	private LogicShape findLogicShape(Shape givenShape)
	{
		//we want to find the LogicShape that has the givenShape as its shape
		for (LogicShape cur: onScreen)
		{
			if (cur.getShape() == givenShape)
			{
				return cur;
			}
		}
		return null;
	}

	//displays the given LogicWorld object in the ui
	//i.e. displays all the shapes such that the premises of the world hold
	private void displayLogicWorld(LogicWorld world)
	{
		//delete all shapes already in the ui
		deleteAllShapes();

		//get the list of shapes from the world
		ArrayList<LogicShape> worldShapes = world.getShapes();

		//display all the shapes
		for (LogicShape curShape: worldShapes)
		{
			displayShape(curShape);
		}

		//we should now have created all the shapes with the correct size
		//we must now satisfy the binary predicates

		//get the horizontal ordering of the shapes
		ShapeSet[] HorOrder = world.getHOrder();
		//now we have to horizontal order of the shapes, position them in this order
		positionShapes(HorOrder, Orientation.HORIZONTAL);

		//now order the shapes vertically
		ShapeSet[] VerOrder = world.getVOrder();
		positionShapes(VerOrder, Orientation.VERTICAL);

	}

	//moves the shapes so that their order matches the order in the array
	private void positionShapes(ShapeSet[] sets, Orientation direction)
	{
		//all shapes are intially at the origin
		//leave the first set where it is

		int noSets = sets.length;
		double offset = 0.0;

		for (int i = 1; i < noSets; i++)
		{
			//add to the offset to the previous shapes radius + a little bit of spacing
			offset += sets[i-1].getHSpacing() + 5.0;

			//move the set to that position
			if (direction == Orientation.HORIZONTAL)
			{
				sets[i].moveBy(offset, 0.0);
			}
			else if (direction == Orientation.VERTICAL)
			{
				sets[i].moveBy(0.0, offset);
			}

		}
	}






	public static void main(String[] args)
	{
		launch(args);
	}
}
