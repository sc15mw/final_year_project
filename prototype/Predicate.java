package prototype;

import java.util.ArrayList;

//the different predicates available
public enum Predicate
{
	//binary predicates
	IS_ABOVE ("isAbove"), 
	IS_BELOW ("isBelow"), 
	IS_LEFT_OF ("isLeftOf"), 
	IS_RIGHT_OF ("isRightOf"), 
	IS_OVERLAPPING ("isOverlapping"), 
	IS_H_ALIGNED ("isHAligned"),
	IS_V_ALIGNED ("isVAligned"),
	IS_INSIDE ("isInside"),

	//unary predicates
	IS_BIG ("isBig"), 
	IS_SMALL ("isSmall"), 
	IS_MED ("isMed"),
	IS_SQUARE ("isSquare"),
	IS_CIRCLE ("isCircle");

	private final String text;

	private final int noPredicates = 13;

	private Predicate(final String givenText)
	{
		this.text = givenText;
	}

	@Override
	public String toString()
	{
		return text;
	}

	//returns the enum value that exactly matches the string given
	public static Predicate fromString(String givenString)
	{
		if (givenString.equals(Predicate.IS_BIG.toString()))
		{
			return IS_BIG;
		}
		else if (givenString.equals(Predicate.IS_MED.toString()))
		{
			return IS_MED;
		}
		else if (givenString.equals(Predicate.IS_SMALL.toString()))
		{
			return IS_SMALL;
		}
		else if (givenString.equals(Predicate.IS_SQUARE.toString()))
		{
			return IS_SQUARE;
		}
		else if (givenString.equals(Predicate.IS_CIRCLE.toString()))
		{
			return IS_CIRCLE;
		}
		else if (givenString.equals(Predicate.IS_ABOVE.toString()))
		{
			return IS_ABOVE;
		}
		else if (givenString.equals(Predicate.IS_BELOW.toString()))
		{
			return IS_BELOW;
		}
		else if (givenString.equals(Predicate.IS_LEFT_OF.toString()))
		{
			return IS_LEFT_OF;
		}
		else if (givenString.equals(Predicate.IS_RIGHT_OF.toString()))
		{
			return IS_RIGHT_OF;
		}
		else if (givenString.equals(Predicate.IS_OVERLAPPING.toString()))
		{
			return IS_OVERLAPPING;
		}

		return null;

	}

	//returns all the predicates in an array
	public static Predicate[] getAll()
	{
		Predicate[] temp = new Predicate[13];
		temp[0] = Predicate.IS_BIG;
		temp[1] = Predicate.IS_MED;
		temp[2] = Predicate.IS_SMALL;
		temp[3] = Predicate.IS_CIRCLE;
		temp[4] = Predicate.IS_SQUARE;
		temp[5] = Predicate.IS_ABOVE;
		temp[6] = Predicate.IS_BELOW;
		temp[7] = Predicate.IS_LEFT_OF;
		temp[8] = Predicate.IS_RIGHT_OF;
		temp[9] = Predicate.IS_OVERLAPPING;
		temp[10] = Predicate.IS_H_ALIGNED;
		temp[11] = Predicate.IS_V_ALIGNED;
		temp[12] = Predicate.IS_INSIDE;

		return temp;
	}
}