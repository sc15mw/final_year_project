package prototype;

//thrown when there is an error with the model
public class ModelException extends Exception
{
	public ModelException(String msg)
	{
		super(msg);
	}
}
