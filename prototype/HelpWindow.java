package prototype;

import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.net.URL;

public class HelpWindow
{
    final String helpPagePath = "/prototype/help/help.html"; //the location of the help HTML page
    private Stage s;
    private Scene helpScene;
    public HelpWindow()
    {
        s = new Stage();
        s.setTitle("Help");

        WebView browser = new WebView();
        WebEngine eng = browser.getEngine();

        URL url = this.getClass().getResource(helpPagePath);
        eng.load(url.toString());

        helpScene = new Scene(browser, 750, 500);
        s.setScene(helpScene);
    }

    public void display()
    {
        s.show();
    }

}
