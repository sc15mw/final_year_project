package prototype;

public enum Quantifier {
    EXISTS("exists"),
    FORALL("all");

    private final String text;

    private Quantifier(final String givenText)
    {
        this.text = givenText;
    }

    @Override
    public String toString()
    {
        return text;
    }

    public static Quantifier[] getAll()
    {
        Quantifier[] temp = new Quantifier[2];
        temp[0] = Quantifier.EXISTS;
        temp[1] = Quantifier.FORALL;

        return temp;
    }
}
