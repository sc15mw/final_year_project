import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import javafx.scene.layout.Pane;
import org.junit.Test;
import prototype.*;

public class LogicCircleTest {
    @Test
    public void size()
    {
        Pane world = new Pane();
        LogicCircle big = new LogicCircle(world, 3);
        LogicCircle med = new LogicCircle(world, 2);
        LogicCircle small = new LogicCircle(world, 1);

        LogicCircle incorrect = new LogicCircle(world, -1);

        assertTrue(big.isBig());
        assertFalse(big.isMed());
        assertFalse(big.isSmall());

        assertFalse(med.isBig());
        assertTrue(med.isMed());
        assertFalse(med.isSmall());

        assertFalse(small.isBig());
        assertFalse(small.isMed());
        assertTrue(small.isSmall());

        assertFalse(incorrect.isBig());
        assertFalse(incorrect.isMed());
        assertTrue(incorrect.isSmall());

    }

}