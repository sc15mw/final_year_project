package tests;

import org.junit.Before;
import org.junit.Test;
import prototype.LogicWorld;
import prototype.Predicate;
import prototype.Premise;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class LogicWorldTest
{
	LogicWorld w;
	Premise addedPrem1;
	Premise addedPrem2;

	@Before
	public void setup()
	{
		w = new LogicWorld("testworld", null);
		addedPrem1 = new Premise(Predicate.IS_RIGHT_OF, "a", "b");
		addedPrem2 = new Premise(Predicate.IS_BIG, "x");
		w.addPremise(addedPrem1);
		w.addPremise(addedPrem2);
	}
	@Test

	public void addGetAllPremise() throws Exception
	{

		ArrayList<Premise> returnedPremises = w.getAllPremises();
		assertTrue(returnedPremises.contains(addedPrem1));
		assertTrue(returnedPremises.contains(addedPrem2));
	}


	@Test
	public void clearPremises() throws Exception
	{
		LogicWorld w2 = new LogicWorld("testworld", null);
		w2.addPremise(new Premise(Predicate.IS_BIG, "y"));
		w2.addPremise(new Premise(Predicate.IS_SQUARE, "c1"));
		w2.addPremise(new Premise("isLeftOf(x,y)."));

		w2.clearPremises();

		ArrayList<Premise> returned = w2.getAllPremises();
		assertTrue(returned.isEmpty());
	}

	@Test
	public void getUnaryPremises() throws Exception
	{
		LogicWorld w3 = new LogicWorld("testworld", null);

		Premise unary1 = new Premise(Predicate.IS_BIG, "x");
		Premise unary2 = new Premise(Predicate.IS_SQUARE, "0ee21ef");
		Premise binary1 = new Premise(Predicate.IS_LEFT_OF, "a", "b");
		Premise binary2 = new Premise(Predicate.IS_BELOW, "x", "d1");

		w3.addPremise(unary1);
		w3.addPremise(unary2);
		w3.addPremise(binary1);
		w3.addPremise(binary2);

		//the list of ALL unary premises
		ArrayList<Premise> returned = w3.getUnaryPremises();
		assertTrue(returned.contains(unary1));
		assertTrue(returned.contains(unary2));
		assertFalse(returned.contains(binary1));
		assertFalse(returned.contains(binary2));

		//lists of unary premises contains "x" (should only be unary1)
		ArrayList<Premise> returned2 = w3.getUnaryPremises("x");
		assertTrue(returned2.contains(unary1));
		assertFalse(returned2.contains(unary2));
		assertFalse(returned2.contains(binary1));
		assertFalse(returned2.contains(binary2));

	}

	@Test
	public void getBinaryPremises() throws Exception
	{
		LogicWorld w3 = new LogicWorld("testworld", null);

		Premise unary1 = new Premise(Predicate.IS_BIG, "x");
		Premise unary2 = new Premise(Predicate.IS_SQUARE, "0ee21ef");
		Premise binary1 = new Premise(Predicate.IS_LEFT_OF, "a", "b");
		Premise binary2 = new Premise(Predicate.IS_BELOW, "x", "d1");

		w3.addPremise(unary1);
		w3.addPremise(unary2);
		w3.addPremise(binary1);
		w3.addPremise(binary2);

		//the list of ALL unary premises
		ArrayList<Premise> returned = w3.getBinaryPremises();
		assertFalse(returned.contains(unary1));
		assertFalse(returned.contains(unary2));
		assertTrue(returned.contains(binary1));
		assertTrue(returned.contains(binary2));

		//lists of unary premises contains "x" (should only be unary1)
		ArrayList<Premise> returned2 = w3.getBinaryPremises("x", "d1");
		assertFalse(returned2.contains(unary1));
		assertFalse(returned2.contains(unary2));
		assertFalse(returned2.contains(binary1));
		assertTrue(returned2.contains(binary2));
	}


	@Test
	public void getNoShapes() throws Exception
	{

	}

	@Test
	public void logicallyEntails() throws Exception
	{

	}

	@Test
	public void getShapes() throws Exception
	{

	}

	@Test
	public void getHOrder() throws Exception
	{

	}

	@Test
	public void getVOrder() throws Exception
	{

	}

}