package tests;

import org.junit.Test;

import prototype.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class PremiseTest {

    //constructor tests also test "toString()"
    @Test
    public void constructor1() throws Exception
    {
        //constructor: 2 args, predicate and arg1
        Premise prem1 = new Premise(Predicate.IS_BIG, "a");
        assertEquals(prem1.toString(), "isBig(a).");

        Premise prem2 = new Premise(Predicate.IS_CIRCLE, "40efdg");
        assertEquals(prem2.toString(), "isCircle(40efdg).");
    }

    @Test
    public void constructor2() throws Exception
    {
        //constructor 3 args, predicate, arg1, arg2
        Premise prem1 = new Premise(Predicate.IS_ABOVE, "a", "b");
        assertEquals(prem1.toString(), "isAbove(a,b).");

        Premise prem2 = new Premise(Predicate.IS_RIGHT_OF, "c1", "0987");
        assertEquals(prem2.toString(), "isRightOf(c1,0987).");
    }

    @Test
    public void constructor3() throws Exception
    {
        //3 args: premise, connective, premise
        Premise prem1 = new Premise(Predicate.IS_BIG, "a");
        Premise prem2 = new Premise(Predicate.IS_RIGHT_OF, "ce", ":D");
        Premise comb = new Premise(prem1, Connective.AND, prem2);

        assertEquals(comb.toString(), "(isBig(a) & isRightOf(ce,:D)).");

        Premise comb2 = new Premise(prem2, Connective.AND, prem1);
        assertEquals(comb2.toString(), "(isRightOf(ce,:D) & isBig(a)).");
        assertNotEquals(comb.toString(), comb2.toString());
    }

    @Test
    public void constructor4() throws Exception
    {
        //2 args: connective, premise
        Premise prem1 = new Premise(Predicate.IS_RIGHT_OF, "a", "b");

        prem1 = new Premise(Connective.NOT, prem1);
        assertEquals(prem1.toString(), "-(isRightOf(a,b)).");

        Premise prem2 = new Premise(Predicate.IS_BIG, "0iei8");
        prem2 = new Premise(Connective.NOT, prem2);

        assertEquals(prem2.toString(), "-(isBig(0iei8)).");

    }
    @Test
    public void constructor5() throws Exception
    {
        //3 args: quantifier, boundVar, premise
        Premise prem1 = new Premise(Predicate.IS_BIG, "a");
        prem1 = new Premise(Quantifier.EXISTS, "a", prem1);

        assertEquals(prem1.toString(), "exists a isBig(a).");

        Premise prem2 = new Premise(Predicate.IS_RIGHT_OF, "x", "y");
        prem2 = new Premise(Quantifier.FORALL, "y", prem2);

        assertEquals(prem2.toString(), "all y isRightOf(x,y).");

        Premise prem3 = new Premise(Quantifier.EXISTS, "x", prem2);
        assertEquals(prem3.toString(), "exists x all y isRightOf(x,y).");

    }
    @Test
    public void constructor6() throws Exception
    {
        //1 arg: premText
        Premise prem1 = new Premise("isBig(a).");
        assertEquals(prem1.toString(), "isBig(a).");

    }

    @Test
    public void getArgs() throws Exception
    {

    }

    @Test
    public void toStringTest() throws Exception {
        Premise prem1 = new Premise(Predicate.IS_BIG, "a");
        Premise prem2 = new Premise(Predicate.IS_MED, "b");
        Premise prem3 = new Premise(Predicate.IS_SMALL, "c");
        Premise prem4 = new Premise(Predicate.IS_CIRCLE, "d");
        Premise prem5 = new Premise(Predicate.IS_SQUARE, "e");

        assertEquals(prem1.toString(), "isBig(a).");
        assertEquals(prem2.toString(), "isMed(b).");
        assertEquals(prem3.toString(), "isSmall(c).");
        assertEquals(prem4.toString(), "isCircle(d).");
        assertEquals(prem5.toString(), "isSquare(e).");

        prem1 = new Premise(Predicate.IS_ABOVE, "a", "b");
        prem2 = new Premise(Predicate.IS_LEFT_OF, "a", "b");
        prem3= new Premise(Predicate.IS_RIGHT_OF, "a", "b");
        prem4 = new Premise(Predicate.IS_BELOW, "a", "b");

        assertEquals(prem1.toString(), "isAbove(a,b).");
        assertEquals(prem2.toString(), "isLeftOf(a,b).");
        assertEquals(prem3.toString(), "isRightOf(a,b).");
        assertEquals(prem4.toString(), "isBelow(a,b).");

        prem1 = new Premise(Predicate.IS_ABOVE, "a", "b");
        prem1 = new Premise(Connective.NOT, prem1);
        assertEquals(prem1.toString(), "-(isAbove(a,b)).");

    }

    @Test
    public void equals() throws Exception {
        Premise prem1 = new Premise(Predicate.IS_ABOVE, "a", "b");
        Premise prem2 = new Premise(Predicate.IS_ABOVE, "b", "a");

        assertTrue(prem1.equals(prem1));
        assertFalse(prem1.equals(prem2));

    }

    @Test
    public void contains() throws Exception {
        String name1 = "hello";
        String name2 = "yo";

        Premise prem = new Premise(Predicate.IS_ABOVE, "hello", "yo");
        assertTrue(prem.contains(name1));
        assertTrue(prem.contains(name2));

        prem = new Premise(Predicate.IS_BIG, "hello");
        assertTrue(prem.contains(name1));
        assertFalse(prem.contains(name2));
    }

    @Test
    public void isUnary() throws Exception {
        Premise unary = new Premise(Predicate.IS_BIG, "a");
        assertTrue(unary.isUnary());

        unary = new Premise(Connective.NOT, unary);
        //assertFalse(unary.isUnary());

        Premise binary = new Premise(Predicate.IS_ABOVE, "a", "b");
        assertFalse(binary.isUnary());

        Premise connected = new Premise(unary, Connective.AND, binary);
        assertFalse(connected.isUnary());

        unary = new Premise(Predicate.IS_SMALL, "c1");
        assertTrue(unary.isUnary());

    }

    @Test
    public void isBinary() throws Exception {
        Premise unary = new Premise(Predicate.IS_BIG, "a");
        assertFalse(unary.isBinary());

        unary = new Premise(Connective.NOT, unary);
        assertFalse(unary.isBinary());

        Premise binary = new Premise(Predicate.IS_ABOVE, "a", "b");
        assertTrue(binary.isBinary());

        Premise connected = new Premise(unary, Connective.AND, binary);
        assertFalse(connected.isBinary());

    }

    @Test
    public void isAtomic() throws Exception
    {
        Premise prem1 = new Premise(Predicate.IS_ABOVE, "a", "b");
        Premise prem2 = new Premise(Predicate.IS_BIG, "a");
        Premise prem4 = new Premise(prem1, Connective.AND, prem2);

        Premise prem5 = new Premise(prem1, Connective.OR, prem2);

        assertTrue(prem1.isAtomic());
        assertTrue(prem2.isAtomic());
        assertFalse(prem4.isAtomic());
        assertFalse(prem5.isAtomic());

        prem1 = new Premise(Predicate.IS_SMALL, "a");
        assertTrue(prem1.isAtomic());
    }

    @Test
    public void containsQuantifier() throws Exception
    {
        Premise a = new Premise(Predicate.IS_ABOVE, "a", "c1");
        Premise prem1 = new Premise(Quantifier.EXISTS, "a", a);
        Premise prem2 = new Premise(Quantifier.FORALL, "c1", a);
        Premise prem3 = new Premise(prem1, Connective.AND, prem2);

        assertTrue(prem1.containsQuantifier(Quantifier.EXISTS));
        assertFalse(prem1.containsQuantifier(Quantifier.FORALL));

        assertTrue(prem2.containsQuantifier(Quantifier.FORALL));
        assertFalse(prem2.containsQuantifier(Quantifier.EXISTS));

        assertTrue(prem3.containsQuantifier(Quantifier.EXISTS));
        assertTrue(prem3.containsQuantifier(Quantifier.FORALL));
    }

    @Test
    public void isValid() throws Exception
    {
        assertTrue(Premise.checkIfValid("isLeftOf(c1,c2)."));
        assertTrue(Premise.checkIfValid("isBig(c1)."));
        assertTrue(Premise.checkIfValid("isAbove(12,4o1e)."));

        assertFalse(Premise.checkIfValid("badPredicate(c1,c2)."));
        assertFalse(Premise.checkIfValid("isLeftOf(c1 c2)."));
        assertFalse(Premise.checkIfValid("isLeftOf(c1,c2)"));



    }

    @Test
    public void getArgs() throws Exception
    {
        Premise prem1 = new Premise("isBig(x, y).");
        ArrayList<String> expectedArguments = new ArrayList<String>();
        expectedArguments.add("x");
        expectedArguments.add("y");

        assertEquals(prem1.getArgs(), expectedArguments);

        Premise prem2 = new Premise("isSmall(4ode2).");
        ArrayList<String> expectedArguments2 = new ArrayList<String>();
        expectedArguments2.add("4ode2");
        assertEquals(prem2.getArgs(), expectedArguments2);
    }

}